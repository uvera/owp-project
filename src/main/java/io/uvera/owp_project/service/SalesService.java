package io.uvera.owp_project.service;

import io.uvera.owp_project.dto.SaleDto;
import io.uvera.owp_project.model.Sale;
import io.uvera.owp_project.repository.SalesRepository;
import lombok.val;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class SalesService {
    private final SalesRepository salesRepository;

    public SalesService(SalesRepository salesRepository) {this.salesRepository = salesRepository;}

    public List<Sale> getSales() {
        return salesRepository.findAll();
    }

    public boolean doesSaleAlreadyExist(LocalDate date) {
        return salesRepository.findBySaleDate(date).isPresent();
    }

    public void addBookSaleFromDto(SaleDto dto) {
        val sale = Sale.builder()
                .percentDown(dto.getPercentDown())
                .saleDate(dto.getSaleDate())
                .build();
        salesRepository.save(sale);
    }
}

package io.uvera.owp_project.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserShopping {
    //spec
    private Long purchaseId;
    private BigDecimal totalPrice;
    private LocalDate dateOfPayment;
    private String username;
    private Long amountOfBooksBought;

    private Set<String> bookSet;
}

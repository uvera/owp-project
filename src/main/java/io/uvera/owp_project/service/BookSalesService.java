package io.uvera.owp_project.service;

import io.uvera.owp_project.dto.BookSaleDto;
import io.uvera.owp_project.model.BookSale;
import io.uvera.owp_project.repository.BookSalesRepository;
import lombok.val;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class BookSalesService {
    private final BookSalesRepository bookSalesRepository;

    public BookSalesService(BookSalesRepository bookSalesRepository) {this.bookSalesRepository = bookSalesRepository;}

    public List<BookSale> getBookSales() {
        return bookSalesRepository.findAll();
    }

    public boolean doesBookSaleAlreadyExists(LocalDate date, String isbn) {
        return bookSalesRepository.findBySaleDateAndBookIsbn(date, isbn).isPresent();
    }

    public void addBookSaleFromDto(BookSaleDto dto) {
        val bookSale = BookSale.builder()
                .percentDown(dto.getPercentDown())
                .saleDate(dto.getSaleDate())
                .bookIsbn(dto.getBookIsbn())
                .build();
        bookSalesRepository.save(bookSale);
    }
}

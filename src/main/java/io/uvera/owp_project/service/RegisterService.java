package io.uvera.owp_project.service;

import io.uvera.owp_project.dto.RegisterUserDTO;
import io.uvera.owp_project.model.User;
import io.uvera.owp_project.repository.UserRepository;
import io.uvera.owp_project.util.OwpConstants;
import lombok.val;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class RegisterService {
    private final UserRepository userRepository;
    private final PasswordEncoder pwe;

    public RegisterService(UserRepository userRepository, PasswordEncoder pwe) {
        this.userRepository = userRepository;
        this.pwe = pwe;
    }

    public boolean doesUserExistByEmail(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    public boolean doesUserExistByUsername(String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    public void registerUserFromDto(RegisterUserDTO dto) {
        val u = User.builder()
                .username(dto.getUsername())
                .password(pwe.encode(dto.getPassword()))
                .email(dto.getEmail())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .dateOfBirth(dto.getDateOfBirth())
                .address(dto.getAddress())
                .phoneNumber(dto.getPhoneNumber())
                .dateOfRegistration(LocalDateTime.now())
                .role(OwpConstants.ROLE_USER)
                .active(true)
                .build();
        userRepository.save(u);
    }
}

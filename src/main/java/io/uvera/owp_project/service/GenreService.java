package io.uvera.owp_project.service;

import io.uvera.owp_project.model.Genre;
import io.uvera.owp_project.repository.GenreRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreService {
    private final GenreRepository genreRepository;

    public GenreService(GenreRepository genreRepository) {this.genreRepository = genreRepository;}

    public List<Genre> getGenres() {
        return genreRepository.findAll();
    }
}

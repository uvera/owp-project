package io.uvera.owp_project.service;

import io.uvera.owp_project.dto.PerformShopDto;
import io.uvera.owp_project.model.Book;
import io.uvera.owp_project.model.BoughtBook;
import io.uvera.owp_project.repository.BookRepository;
import io.uvera.owp_project.repository.LoyaltyCardRepository;
import io.uvera.owp_project.repository.SalesRepository;
import io.uvera.owp_project.repository.ShopRepository;
import io.uvera.owp_project.session.ShoppingCartSession;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShopService {
    private final ShoppingCartSession shoppingCartSession;
    private final BookRepository bookRepository;
    private final ShopRepository shopRepository;
    private final LoyaltyCardRepository loyaltyCardRepository;
    private final SalesRepository salesRepository;

    public ShopService(ShoppingCartSession shoppingCartSession, BookRepository bookRepository, ShopRepository shopRepository, LoyaltyCardRepository loyaltyCardRepository, SalesRepository salesRepository) {
        this.shoppingCartSession = shoppingCartSession;
        this.bookRepository = bookRepository;
        this.shopRepository = shopRepository;
        this.loyaltyCardRepository = loyaltyCardRepository;
        this.salesRepository = salesRepository;
    }

    public boolean executePurchase(PerformShopDto dto, String username) {

        // get today sale optionally
        val todaySale = salesRepository.findSaleToday();

        // get loyalty card optionally
        val loyaltyCard = loyaltyCardRepository.findByUsername(username);

        // if loyalty card is not present but user wants to use points cancel, otherwise continue
        if (loyaltyCard.isEmpty() && dto.getUseAmountOfPoints() > 0) {
            return false;
        }

        // if loyalty card is present check if there are enough points, otherwise cancel
        if (loyaltyCard.isPresent() && loyaltyCard.get().getPoints() < dto.getUseAmountOfPoints()) {
            return false;
        }

        // map items from cart as a list of book's ISBN
        val booksFromSessionIsbnList = shoppingCartSession.getCart()
                .stream()
                .map(ShoppingCartSession.Item::getBookIsbn)
                .collect(Collectors.toList());

        // find all books by previous books from session computation
        val booksApplySale = bookRepository.findAllByIsbn(booksFromSessionIsbnList);

        // find if there are books with percent sale going on (today)
        val isThereBookWithPercentSale = booksApplySale.stream().anyMatch(e -> e.getPercentIntSale() > 0);

        /* If there is no sale today
         * and there are no books with percent sale today
         * and user owns a loyalty card
         * go ahead and apply loyalty card points
         */
        var purchaseWithPoints = false;
        if (todaySale.isEmpty() && !isThereBookWithPercentSale && loyaltyCard.isPresent() && dto.getUseAmountOfPoints() > 0) {
            purchaseWithPoints = true;
            booksApplySale.forEach(e -> e.setPercentIntSale(dto.getUseAmountOfPoints() * 5));
        }
        /* Otherwise there's a sale today, books with percent sale and no loyalty card
         * apply today sale or keep percent sale
         */
        else if (todaySale.isPresent()) {
            val todaySaleAmount = todaySale.get().getPercentDown();
            booksApplySale.stream().filter(e -> e.getPercentIntSale() < todaySaleAmount)
                    .forEach(e -> e.setPercentIntSale(todaySaleAmount));

        }
        // map books from apply sale
        val books = booksApplySale
                .stream()
                .collect(Collectors.toUnmodifiableMap(Book::getIsbn, e -> e));

        // build bought books to pass to the repository, get price from books map
        val boughtBooks = shoppingCartSession.getCart().stream()
                .map(e -> BoughtBook.builder()
                        .amountOfBooks(e.getAmount())
                        .bookIsbn(e.getBookIsbn())
                        .price(books.get(e.getBookIsbn()).getPrice())
                        .build())
                .collect(Collectors.toList());

        // perform purchase and remove points used on line 69
        if (purchaseWithPoints) {
            shopRepository.performPurchase(boughtBooks, username, dto.getUseAmountOfPoints());
        }
        // otherwise just perform purchase
        else {
            shopRepository.performPurchase(boughtBooks, username);
        }
        // remove books from session
        shoppingCartSession.dropCart();

        return true;
    }

    public boolean checkBooksAvailability() {
        // get books from session and convert to map
        val booksFromSessionMap = shoppingCartSession.getCart()
                .stream()
                .collect(Collectors.toUnmodifiableMap(ShoppingCartSession.Item::getBookIsbn, e -> e));

        // get isbn list (string)
        val booksIsbnList = new ArrayList<>(booksFromSessionMap.keySet());

        // find all books by those from session component
        val books = bookRepository.findAllByIsbn(booksIsbnList);

        // find books that cannot be purchased
        val booksThatCannotBePurchased = books.stream()
                .filter(e -> e.getAvailableAmount() < booksFromSessionMap.get(e.getIsbn()).getAmount())
                .collect(Collectors.toList());

        val status = booksThatCannotBePurchased.isEmpty();

        // edit those books in session, change their selected amount
        booksThatCannotBePurchased.forEach(e -> shoppingCartSession
                .editCartItem(e.getIsbn(), Long.valueOf(e.getAvailableAmount())));

        return status;
    }

    public List<UserShopping> getUserShoppingsByUsername(String username) {
        return shopRepository.findUserShoppingsByUsername(username);
    }

    public UserShopping getUserShoppingById(Long id) {
        return shopRepository.findUserShoppingById(id);
    }
}

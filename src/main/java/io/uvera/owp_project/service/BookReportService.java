package io.uvera.owp_project.service;

import io.uvera.owp_project.dto.SearchReportDTO;
import io.uvera.owp_project.model.BookReport;
import io.uvera.owp_project.repository.BookReportRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookReportService {
    private final BookReportRepository bookReportRepository;

    public BookReportService(BookReportRepository bookReportRepository) {this.bookReportRepository = bookReportRepository;}

    public List<BookReport> getAllReportsFromDto(SearchReportDTO dto) {
        return bookReportRepository.findAllByStartDateAndEndDate(dto.getStartDate(), dto.getEndDate());
    }
}

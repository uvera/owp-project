package io.uvera.owp_project.service;

import io.uvera.owp_project.dto.AdminLoyaltyCardRequestDTO;
import io.uvera.owp_project.model.LoyaltyCard;
import io.uvera.owp_project.model.LoyaltyCardRequest;
import io.uvera.owp_project.model.User;
import io.uvera.owp_project.repository.LoyaltyCardRepository;
import io.uvera.owp_project.repository.LoyaltyCardRequestRepository;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LoyaltyCardRequestService {
    private final LoyaltyCardRequestRepository loyaltyCardRequestRepository;
    private final LoyaltyCardRepository loyaltyCardRepository;

    public LoyaltyCardRequestService(LoyaltyCardRequestRepository loyaltyCardRequestRepository, LoyaltyCardRepository loyaltyCardRepository) {
        this.loyaltyCardRequestRepository = loyaltyCardRequestRepository;
        this.loyaltyCardRepository = loyaltyCardRepository;
    }

    public List<LoyaltyCardRequest> getAllRequests() {
        return loyaltyCardRequestRepository.findAll();
    }

    public boolean canUserRequestLoyaltyCard(String username) {
        return loyaltyCardRequestRepository.findByUsernameAndStatus_PendingOrAccepted(username).isEmpty();
    }

    public void submitRequestForUsername(String name) {
        val req = LoyaltyCardRequest.builder()
                .user(User.builder()
                              .username(name)
                              .build())
                .status(LoyaltyCardRequest.Status.PENDING)
                .build();
        loyaltyCardRequestRepository.save(req);

    }

    public boolean doesRequestExistByUsername(String obj) {
        return loyaltyCardRequestRepository.findByUsername(obj).isPresent();
    }

    public void performUpdateFromDto(AdminLoyaltyCardRequestDTO dto) {
        val req = LoyaltyCardRequest.builder()
                .user(User.builder()
                              .username(dto.getUsername())
                              .build())
                .status(dto.getAction() == AdminLoyaltyCardRequestDTO.Action.ACCEPT ? LoyaltyCardRequest.Status.ACCEPTED :
                                LoyaltyCardRequest.Status.REJECTED)
                .build();
        loyaltyCardRequestRepository.save(req);
        if (req.getStatus() == LoyaltyCardRequest.Status.ACCEPTED)
            createLoyaltyCardForUser(dto.getUsername());
    }

    private void createLoyaltyCardForUser(String username) {
        val lc = LoyaltyCard.builder()
                .username(username)
                .build();
        loyaltyCardRepository.save(lc);
    }

    public Optional<LoyaltyCardRequest> getByUsername(String obj) {
        return loyaltyCardRequestRepository.findByUsername(obj);
    }
}

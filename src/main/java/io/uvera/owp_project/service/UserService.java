package io.uvera.owp_project.service;

import io.uvera.owp_project.model.User;
import io.uvera.owp_project.repository.UserRepository;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {this.userRepository = userRepository;}

    public Optional<User> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }


    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void updateRole(String username, String strRole) {
        userRepository.updateUserRoleByUsername(username, strRole);
    }

    public boolean isUserAdmin(String obj) {
        val user = userRepository.findByUsername(obj);
        return user.map(value -> value.getRole().equalsIgnoreCase("admin")).orElse(false);
    }

    public void updateActive(String username, boolean active) {
        userRepository.updateUserActiveByUsername(username, active);
    }
}

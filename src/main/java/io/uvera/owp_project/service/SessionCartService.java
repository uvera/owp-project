package io.uvera.owp_project.service;

import io.uvera.owp_project.model.Book;
import io.uvera.owp_project.model.CartBook;
import io.uvera.owp_project.repository.BookRepository;
import io.uvera.owp_project.session.ShoppingCartSession;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SessionCartService {
    private final ShoppingCartSession shoppingCartSession;
    private final BookRepository bookRepository;

    public SessionCartService(ShoppingCartSession shoppingCartSession, BookRepository bookRepository) {
        this.shoppingCartSession = shoppingCartSession;
        this.bookRepository = bookRepository;
    }

    public void addToCartByIsbn(String isbn) {
        shoppingCartSession.addToCart(ShoppingCartSession.Item
                                              .builder()
                                              .amount(1L)
                                              .bookIsbn(isbn)
                                              .build());
    }

    public boolean changeCartItem(String isbn, Long amount) {
        return shoppingCartSession.editCartItem(isbn, amount);
    }

    public boolean deleteCartItem(String isbn) {
        return shoppingCartSession.removeFromCart(isbn);
    }

    public List<CartBook> getCartBooks() {
        val cartSessionBookIsbns = shoppingCartSession.getCart()
                .stream()
                .map(ShoppingCartSession.Item::getBookIsbn)
                .collect(Collectors.toList());

        if (cartSessionBookIsbns.isEmpty())
            return new ArrayList<>();

        val books = bookRepository.findAllByIsbn(cartSessionBookIsbns);

        val mapBooks = books.stream()
                .collect(Collectors.toUnmodifiableMap(Book::getIsbn, e -> e));

        return shoppingCartSession.getCart()
                .stream()
                .map(e -> CartBook.builder()
                        .book(mapBooks.get(e.getBookIsbn()))
                        .amount(e.getAmount())
                        .build())
                .collect(Collectors.toList());

    }
}

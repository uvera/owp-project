package io.uvera.owp_project.service;

import io.uvera.owp_project.dto.AdminCommentReviewDto;
import io.uvera.owp_project.dto.CommentDto;
import io.uvera.owp_project.model.Comment;
import io.uvera.owp_project.repository.CommentRepository;
import lombok.val;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class CommentService {
    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {this.commentRepository = commentRepository;}

    public boolean canComment(String isbn, String name) {
        return commentRepository.canComment(isbn, name);
    }

    public void saveComment(String isbn, String name, CommentDto dto) {
        val comment = Comment.builder()
                .text(dto.getText())
                .rating(dto.getRating())
                .author(name)
                .bookIsbn(isbn)
                .status(Comment.Status.PENDING)
                .dateOfPosting(LocalDate.now())
                .build();
        commentRepository.save(comment);
    }

    public List<Comment> getAllCommentsPending() {
        return commentRepository.getAllCommentsToReview();
    }

    public boolean canReviewComment(AdminCommentReviewDto dto) {
        return commentRepository.findByAuthorAndBookIsbnAndStatusIsThere(dto.getAuthor(), dto.getBookIsbn(),
                                                                         Comment.Status.PENDING);
    }

    public void updateComment(AdminCommentReviewDto dto) {
        val status = dto.getAccept() ? Comment.Status.ACCEPTED : Comment.Status.REJECTED;
        commentRepository.updateByAuthorAndBookIsbnWithStatus(dto.getAuthor(), dto.getBookIsbn(), status);
    }
}

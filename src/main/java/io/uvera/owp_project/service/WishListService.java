package io.uvera.owp_project.service;

import io.uvera.owp_project.model.WishListItem;
import io.uvera.owp_project.repository.WishListRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WishListService {
    private final WishListRepository wishListRepository;

    public WishListService(WishListRepository wishListRepository) {this.wishListRepository = wishListRepository;}

    public List<WishListItem> getWishListForUsername(String username) {
        return wishListRepository.findWishListForUsername(username);
    }

    public void insertWishListItemByIsbnAndUsername(String isbn, String name) {
        wishListRepository.insertWishListItemByIsbnAndUsername(isbn, name);
    }

    public void removeWishListItemForUsernameAndIsbn(String name, String isbn) {
        wishListRepository.remove(name, isbn);
    }
}

package io.uvera.owp_project.service;

import io.uvera.owp_project.dto.BookAddDTO;
import io.uvera.owp_project.dto.BookChangeDTO;
import io.uvera.owp_project.dto.SearchBookDTO;
import io.uvera.owp_project.model.Book;
import io.uvera.owp_project.repository.BookRepository;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookService {
    private static final String PROJECT_PATH = System.getProperty("user.dir");
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {this.bookRepository = bookRepository;}

    public List<Book> getAvailableBooks() {
        return bookRepository.findAllAvailable();
    }

    public List<Book> searchAvailableBooksByDto(SearchBookDTO dto) {
        return bookRepository.findAllAvailableBySearchParameters(dto.getName(), dto.getGenres(),
                                                                 dto.getPriceMin(), dto.getPriceMax(),
                                                                 dto.getAuthor(), dto.getLanguage());
    }

    public Optional<Book> getBookByIsbnIfAvailable(String isbn) {
        return bookRepository.findAvailableByIsbn(isbn);
    }

    public Optional<Book> getBookByIsbn(String isbn) {
        return bookRepository.findByIsbn(isbn);
    }

    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    public boolean updateBook(Book book, BookChangeDTO dto) {
        val file = dto.getImageFile();
        if (file != null && !file.isEmpty() && file.getContentType() != null) {
            val contentType = file.getContentType();
            if (contentType.equalsIgnoreCase("image/jpeg"))
                saveJpeg(file, book.getIsbn());
            else if (contentType.equalsIgnoreCase("image/png"))
                savePng(file, book.getIsbn());
            else return false;
        }
        updateBookFieldsWithDto(book, dto);
        return true;
    }

    @SneakyThrows
    private void saveJpeg(MultipartFile file, String isbn) {
        createDir();
        val path = PROJECT_PATH + "/images/" + isbn + ".jpg";
        File outputFile = new File(path);
        if (!outputFile.exists())
            Files.createFile(Paths.get(path));
        file.transferTo(outputFile);
    }

    @SneakyThrows
    private void savePng(MultipartFile file, String isbn) {
        createDir();
        val path = PROJECT_PATH + "/images/" + isbn + ".jpg";
        val fromFile = ImageIO.read(file.getInputStream());
        val jpeg = new BufferedImage(
                fromFile.getWidth(), fromFile.getHeight(), BufferedImage.TYPE_INT_RGB
        );
        jpeg.createGraphics().drawImage(fromFile, 0, 0, Color.WHITE, null);
        File outputFile = new File(path);
        if (!outputFile.exists())
            Files.createFile(Paths.get(path));
        ImageIO.write(jpeg, "jpg", outputFile);
    }

    private void updateBookFieldsWithDto(Book book, BookChangeDTO dto) {
        book.setName(dto.getName());
        book.setPublishingHouse(dto.getPublishingHouse());
        book.setYearOfRelease(String.valueOf(dto.getYearOfRelease()));
        book.setDescription(dto.getDescription());
        book.setPrice(dto.getPrice());
        book.setNumOfPages(dto.getNumOfPages());
        book.setTypeOfWrap(dto.getTypeOfWrap());
        book.setTypeOfLetter(dto.getTypeOfLetter());
        book.setLanguage(dto.getLanguage());
        bookRepository.updateBookWithGenresAndAuthors(book, dto.getGenres(),
                                                      Arrays.stream(dto.getAuthors().split(",")).map(String::trim).collect(Collectors.toList()));
    }

    @SneakyThrows
    private void createDir() {
        if (Files.notExists(Path.of(PROJECT_PATH + "/images")))
            Files.createDirectory(Path.of(PROJECT_PATH + "/images"));
    }

    public void orderForBook(Book book, Integer value) {
        bookRepository.updateBooksAmountAvailable(book, value);
    }

    public boolean saveBook(BookAddDTO dto) {
        val file = dto.getImageFile();
        if (file != null && !file.isEmpty() && file.getContentType() != null) {
            val contentType = file.getContentType();
            val isbn = dto.getIsbn();
            if (contentType.equalsIgnoreCase("image/jpeg"))
                saveJpeg(file, isbn);
            else if (contentType.equalsIgnoreCase("image/png"))
                savePng(file, isbn);
            else return false;
        } else {
            return false;
        }
        val authors =
                Arrays.stream(dto.getAuthors().split(",")).distinct().map(String::trim).filter(e -> !e.isBlank())
                        .collect(Collectors.toList());

        if (authors.isEmpty())
            return false;

        val book = Book.builder()
                .name(dto.getName())
                .isbn(dto.getIsbn())
                .publishingHouse(dto.getPublishingHouse())
                .yearOfRelease(String.valueOf(dto.getYearOfRelease()))
                .description(dto.getDescription())
                .price(dto.getPrice())
                .numOfPages(dto.getNumOfPages())
                .typeOfLetter(dto.getTypeOfLetter())
                .typeOfWrap(dto.getTypeOfWrap())
                .language(dto.getLanguage())
                .build();
        bookRepository.saveBookWithGenresAndAuthors(book, dto.getGenres(), authors);
        return true;
    }
}

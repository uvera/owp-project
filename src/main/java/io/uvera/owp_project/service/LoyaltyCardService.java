package io.uvera.owp_project.service;

import io.uvera.owp_project.model.LoyaltyCard;
import io.uvera.owp_project.repository.LoyaltyCardRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoyaltyCardService {
    private final LoyaltyCardRepository loyaltyCardRepository;

    public LoyaltyCardService(LoyaltyCardRepository loyaltyCardRepository) {this.loyaltyCardRepository = loyaltyCardRepository;}

    public Optional<LoyaltyCard> getLoyaltyCardByUsername(String username) {
        return loyaltyCardRepository.findByUsername(username);
    }
}

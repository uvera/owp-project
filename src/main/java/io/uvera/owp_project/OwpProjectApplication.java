package io.uvera.owp_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OwpProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(OwpProjectApplication.class, args);
    }


}

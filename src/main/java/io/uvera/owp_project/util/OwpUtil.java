package io.uvera.owp_project.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class OwpUtil {
    private OwpUtil() {}

    public static boolean hasAuthority(Authentication authentication, String authority) {
        if (authentication == null || authority == null)
            return false;
        return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).anyMatch(e -> e.equals(
                authority));
    }
}

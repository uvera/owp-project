package io.uvera.owp_project.util;

public class OwpConstants {
    public static final String ROLE_USER = "USER";
    public static final String ROLE_ADMIN = "ADMIN";
    public static final int YEAR = 2020;

}

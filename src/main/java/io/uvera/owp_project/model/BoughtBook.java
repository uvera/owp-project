package io.uvera.owp_project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BoughtBook {
    private String bookIsbn;
    private Long amountOfBooks;
    private Double price;
}

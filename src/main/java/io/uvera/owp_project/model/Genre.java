package io.uvera.owp_project.model;

import lombok.Data;

@Data
public class Genre {
    private Long id;
    private String name;
    private String description;
}

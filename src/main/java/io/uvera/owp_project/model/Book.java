package io.uvera.owp_project.model;

import lombok.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Book {
    private String name;
    private String isbn;
    private String publishingHouse;
    private Set<String> authors;
    private String yearOfRelease;
    private String description;
    private Double price;
    private Integer numOfPages;
    private TypeOfWrap typeOfWrap;
    private TypeOfLetter typeOfLetter;
    private String language;
    // Sale percent represented as INT range of 0 to 100, if 0 it's not on sale
    private int percentIntSale;
    private Set<Genre> genres;
    private Integer availableAmount;
    private Set<Comment> comments;

    public Double getPrice() {
        return this.price * (100 - percentIntSale) / 100;
    }

    public void addAuthor(String author) {
        this.authors.add(author);
    }

    public void addGenre(Genre genre) {
        this.genres.add(genre);
    }

    public void addComment(Comment com) {
        this.comments.add(com);
    }

    public String getAllAuthors() {
        val arrayList = new ArrayList<>(this.authors);
        Collections.sort(arrayList);
        return String.join(", ", arrayList);
    }

    public String getAllGenres() {
        val arrayList = new ArrayList<>(this.genres).stream().map(Genre::getName)
                .collect(Collectors.toList());

        return String.join(", ", arrayList);
    }

    public List<Long> getGenresIds() {
        return this.genres.stream().map(Genre::getId).collect(Collectors.toList());
    }

    public Double getAverageScore() {
        BigDecimal bd = BigDecimal.valueOf(this.comments.stream()
                                                   .mapToDouble(Comment::getRating)
                                                   .average()
                                                   .orElse(0.0));
        bd = bd.setScale(2, RoundingMode.HALF_EVEN);
        return bd.doubleValue();
    }

    public enum TypeOfWrap {
        HARD, SOFT
    }

    public enum TypeOfLetter {
        LATIN, CYRILLIC
    }
}

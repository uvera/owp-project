package io.uvera.owp_project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Comment {
    private String author;
    private String text;
    private Integer rating;
    private LocalDate dateOfPosting;
    private String bookIsbn;
    private Status status;
    private String bookName;

    public enum Status {
        PENDING, ACCEPTED, REJECTED
    }
}

package io.uvera.owp_project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookReport {
    private String bookIsbn;
    private String bookName;
    private Set<String> authors;
    private BigDecimal amountSold;
    private BigDecimal totalPrice;

    public String getAuthorsAsString() {
        return String.join(", ", authors);
    }
}

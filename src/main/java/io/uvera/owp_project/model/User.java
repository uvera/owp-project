package io.uvera.owp_project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String address;
    private String phoneNumber;
    private LocalDateTime dateOfRegistration;
    private String role;
    private boolean active;

    public Map<String, Object> toMap() {
        return Map.ofEntries
                (Map.entry("username", this.username),
                 Map.entry("password", this.password),
                 Map.entry("email", this.email),
                 Map.entry("firstName", this.firstName),
                 Map.entry("lastName", this.lastName),
                 Map.entry("dateOfBirth", this.dateOfBirth),
                 Map.entry("address", this.address),
                 Map.entry("phoneNumber", this.phoneNumber),
                 Map.entry("dateOfRegistration", this.dateOfRegistration),
                 Map.entry("role", this.role),
                 Map.entry("active", this.active));
    }
}

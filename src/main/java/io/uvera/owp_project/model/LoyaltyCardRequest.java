package io.uvera.owp_project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoyaltyCardRequest {
    private User user;
    private Status status;

    public enum Status {
        PENDING, ACCEPTED, REJECTED
    }
}

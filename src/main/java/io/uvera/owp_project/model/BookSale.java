package io.uvera.owp_project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookSale {
    private Byte percentDown;
    private LocalDate saleDate;
    private String bookIsbn;
}

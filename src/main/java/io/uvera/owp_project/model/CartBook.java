package io.uvera.owp_project.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CartBook {
    private Book book;
    @Getter
    private Long amount;

    public Double getTotalPrice() {return this.getPrice() * this.amount;}

    public Double getPrice() {return this.book.getPrice();}

    public String getName() {
        return this.book.getName();
    }

    public String getAuthors() {
        return this.book.getAllAuthors();
    }

    public String getIsbn() {
        return this.book.getIsbn();
    }

}

package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.UniqueEmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueEmailValidator.class)
public @interface UniqueEmail {
    String message() default "E-mail already exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

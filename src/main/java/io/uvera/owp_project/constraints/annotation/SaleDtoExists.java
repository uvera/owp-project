package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.SaleDtoExistsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SaleDtoExistsValidator.class)
public @interface SaleDtoExists {
    String message() default "{SaleDto.exists}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

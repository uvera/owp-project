package io.uvera.owp_project.constraints.annotation;


import io.uvera.owp_project.constraints.validator.FieldsShouldMatchValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FieldsShouldMatchValidator.class)
public @interface FieldsShouldMatch {
    String message() default "Fields do not match";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String[] fields();


    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @interface Series {
        FieldsShouldMatch[] value();
    }
}

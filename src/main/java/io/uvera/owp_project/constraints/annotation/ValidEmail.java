package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.ValidEmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidEmailValidator.class)
public @interface ValidEmail {
    String message() default "E-mail isn't valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}

package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.BookExistsByIsbnValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BookExistsByIsbnValidator.class)
public @interface BookExistsByIsbn {
    String message() default "{book.exists.by.isbn}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

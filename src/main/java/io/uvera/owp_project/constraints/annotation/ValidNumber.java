package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.ValidNumberValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidNumberValidator.class)
public @interface ValidNumber {
    String message() default "Number isn't valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

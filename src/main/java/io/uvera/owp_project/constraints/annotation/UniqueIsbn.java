package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.UniqueIsbnValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueIsbnValidator.class)
public @interface UniqueIsbn {
    String message() default "ISBN not unique";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

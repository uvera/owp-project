package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.LegitDateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = LegitDateValidator.class)
public @interface LegitDate {
    String message() default "Date is not in the valid range";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String startDate() default "01-01-1900";

    String endDate() default "01-01-2007";

}

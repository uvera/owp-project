package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.LCRAcceptedAlreadyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = LCRAcceptedAlreadyValidator.class)
public @interface LCRAcceptedAlready {
    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

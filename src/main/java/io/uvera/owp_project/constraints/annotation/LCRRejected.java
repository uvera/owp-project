package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.LCRRejectedValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = LCRRejectedValidator.class)
public @interface LCRRejected {
    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package io.uvera.owp_project.constraints.annotation;

import io.uvera.owp_project.constraints.validator.BookSaleDtoExistsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BookSaleDtoExistsValidator.class)
public @interface BookSaleDtoExists {
    String message() default "{bookSaleDto.exists}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.LCRAcceptedAlready;
import io.uvera.owp_project.model.LoyaltyCardRequest;
import io.uvera.owp_project.service.LoyaltyCardRequestService;
import lombok.val;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LCRAcceptedAlreadyValidator implements ConstraintValidator<LCRAcceptedAlready, String> {
    private final LoyaltyCardRequestService loyaltyCardRequestService;

    public LCRAcceptedAlreadyValidator(LoyaltyCardRequestService loyaltyCardRequestService) {this.loyaltyCardRequestService = loyaltyCardRequestService;}

    public boolean isValid(String obj, ConstraintValidatorContext context) {
        boolean retval = true;
        val lcr = loyaltyCardRequestService.getByUsername(obj);
        if (lcr.isPresent() && lcr.get().getStatus() == LoyaltyCardRequest.Status.ACCEPTED)
            retval = false;
        return retval;
    }

}

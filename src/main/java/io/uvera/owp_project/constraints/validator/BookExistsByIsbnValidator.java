package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.BookExistsByIsbn;
import io.uvera.owp_project.service.BookService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BookExistsByIsbnValidator implements ConstraintValidator<BookExistsByIsbn, String> {
    private final BookService bookService;

    public BookExistsByIsbnValidator(BookService bookService) {this.bookService = bookService;}

    public boolean isValid(String obj, ConstraintValidatorContext context) {
        return bookService.getBookByIsbn(obj).isPresent();
    }
}

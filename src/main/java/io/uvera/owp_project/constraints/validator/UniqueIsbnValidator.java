package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.UniqueIsbn;
import io.uvera.owp_project.service.BookService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueIsbnValidator implements ConstraintValidator<UniqueIsbn, String> {
    private final BookService bookService;

    public UniqueIsbnValidator(BookService bookService) {this.bookService = bookService;}

    public boolean isValid(String isbn, ConstraintValidatorContext context) {
        return bookService.getBookByIsbn(isbn.trim()).isEmpty();
    }
}

package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.FieldsShouldMatch;
import lombok.val;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class FieldsShouldMatchValidator implements ConstraintValidator<FieldsShouldMatch, Object> {
    private String[] fields;

    @Override
    public void initialize(FieldsShouldMatch constraintAnnotation) {
        this.fields = constraintAnnotation.fields();
    }


    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        val bw = new BeanWrapperImpl(obj);
        return Arrays.stream(fields).map(bw::getPropertyValue).distinct().count() <= 1;
    }
}

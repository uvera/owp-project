package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.SaleDtoExists;
import io.uvera.owp_project.service.SalesService;
import lombok.val;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class SaleDtoExistsValidator implements ConstraintValidator<SaleDtoExists, Object> {
    private final SalesService salesService;

    public SaleDtoExistsValidator(SalesService salesService) {this.salesService = salesService;}

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        val bw = new BeanWrapperImpl(value);
        val saleDate = (LocalDate) bw.getPropertyValue("saleDate");
        return !salesService.doesSaleAlreadyExist(saleDate);

    }
}

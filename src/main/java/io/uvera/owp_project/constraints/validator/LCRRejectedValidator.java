package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.LCRRejected;
import io.uvera.owp_project.model.LoyaltyCardRequest;
import io.uvera.owp_project.service.LoyaltyCardRequestService;
import lombok.val;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LCRRejectedValidator implements ConstraintValidator<LCRRejected, String> {
    private final LoyaltyCardRequestService loyaltyCardRequestService;

    public LCRRejectedValidator(LoyaltyCardRequestService loyaltyCardRequestService) {this.loyaltyCardRequestService = loyaltyCardRequestService;}

    public boolean isValid(String obj, ConstraintValidatorContext context) {
        boolean retval = true;
        val lcr = loyaltyCardRequestService.getByUsername(obj);
        if (lcr.isPresent() && lcr.get().getStatus() == LoyaltyCardRequest.Status.REJECTED)
            retval = false;
        return retval;
    }

}

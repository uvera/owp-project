package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.UniqueUsername;
import io.uvera.owp_project.service.RegisterService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {

    private final RegisterService registerService;

    public UniqueUsernameValidator(RegisterService registerService) {this.registerService = registerService;}

    public boolean isValid(String username, ConstraintValidatorContext context) {
        return !registerService.doesUserExistByUsername(username);
    }
}

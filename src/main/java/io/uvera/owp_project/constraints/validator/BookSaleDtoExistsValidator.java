package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.BookSaleDtoExists;
import io.uvera.owp_project.service.BookSalesService;
import lombok.val;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class BookSaleDtoExistsValidator implements ConstraintValidator<BookSaleDtoExists, Object> {
    private final BookSalesService bookSalesService;

    public BookSaleDtoExistsValidator(BookSalesService bookSalesService) {this.bookSalesService = bookSalesService;}

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        val bw = new BeanWrapperImpl(value);
        val saleDate = (LocalDate) bw.getPropertyValue("saleDate");
        val bookIsbn = (String) bw.getPropertyValue("bookIsbn");
        return !bookSalesService.doesBookSaleAlreadyExists(saleDate, bookIsbn);

    }
}

package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.LegitDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LegitDateValidator implements ConstraintValidator<LegitDate, LocalDate> {

    private LocalDate startDate;
    private LocalDate endDate;

    @Override
    public void initialize(LegitDate constraint) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        startDate = LocalDate.parse(constraint.startDate(), dtf);
        endDate = LocalDate.parse(constraint.endDate(), dtf);
    }

    public boolean isValid(LocalDate dateField, ConstraintValidatorContext context) {
        return (!dateField.isBefore(startDate) && dateField.isBefore(endDate));
    }
}

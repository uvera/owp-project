package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.ValidEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class ValidEmailValidator implements ConstraintValidator<ValidEmail, String> {
    //taken from https://www.w3spoint.com/validate-email-regular-expression-regex-java
    public static final Pattern EMAIL_REGEX = Pattern.compile(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    );

    public boolean isValid(String field, ConstraintValidatorContext context) {
        return EMAIL_REGEX.matcher(field).matches();
    }
}

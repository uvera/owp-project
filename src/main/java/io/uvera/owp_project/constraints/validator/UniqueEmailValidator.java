package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.UniqueEmail;
import io.uvera.owp_project.service.RegisterService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

    private final RegisterService registerService;

    public UniqueEmailValidator(RegisterService registerService) {this.registerService = registerService;}

    public boolean isValid(String email, ConstraintValidatorContext context) {
        return !registerService.doesUserExistByEmail(email);
    }
}

package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.ValidNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class ValidNumberValidator implements ConstraintValidator<ValidNumber, String> {
    // taken from https://www.baeldung.com/java-regex-validate-phone-numbers
    private static final Pattern PHONE_NUMBER_REGEX =
            Pattern.compile
                    (
                            "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$"
                                    + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?){2}\\d{3}$"
                                    + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?)(\\d{2}[ ]?){2}\\d{2}$"
                    );


    public boolean isValid(String field, ConstraintValidatorContext context) {
        return PHONE_NUMBER_REGEX.matcher(field).matches();
    }

}

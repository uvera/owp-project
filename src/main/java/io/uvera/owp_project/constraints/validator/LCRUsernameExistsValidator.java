package io.uvera.owp_project.constraints.validator;

import io.uvera.owp_project.constraints.annotation.LCRUsernameExists;
import io.uvera.owp_project.service.LoyaltyCardRequestService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LCRUsernameExistsValidator implements ConstraintValidator<LCRUsernameExists, String> {
    private final LoyaltyCardRequestService loyaltyCardRequestService;

    public LCRUsernameExistsValidator(LoyaltyCardRequestService loyaltyCardRequestService) {this.loyaltyCardRequestService = loyaltyCardRequestService;}

    public boolean isValid(String obj, ConstraintValidatorContext context) {
        return loyaltyCardRequestService.doesRequestExistByUsername(obj);
    }
}

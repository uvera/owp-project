package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.RegisterUserDTO;
import io.uvera.owp_project.service.RegisterService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
public class RegisterController {
    private final RegisterService registerService;

    public RegisterController(RegisterService registerService) {this.registerService = registerService;}

    @GetMapping
    public ModelAndView get() {
        val mav = new ModelAndView("register");
        mav.addObject("formObj", new RegisterUserDTO());
        return mav;
    }

    @PostMapping
    public ModelAndView post(@Valid @ModelAttribute RegisterUserDTO dto, BindingResult result) {
        val mav = new ModelAndView();
        if (!result.hasErrors()) {
            registerService.registerUserFromDto(dto);
            mav.setViewName("redirect:/login");
        } else {
            mav.setViewName("register");
            mav.addObject("formObj", dto);
            mav.addObject("bindingErrors", result.getAllErrors());
        }
        return mav;

    }
}

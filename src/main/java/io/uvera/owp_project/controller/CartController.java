package io.uvera.owp_project.controller;

import io.uvera.owp_project.service.LoyaltyCardService;
import io.uvera.owp_project.service.SessionCartService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping("/cart")
public class CartController {
    private final SessionCartService sessionCartService;
    private final LoyaltyCardService loyaltyCardService;

    public CartController(SessionCartService sessionCartService, LoyaltyCardService loyaltyCardService) {
        this.sessionCartService = sessionCartService;
        this.loyaltyCardService = loyaltyCardService;
    }

    @GetMapping
    public ModelAndView get(Principal principal) {
        val username = principal.getName();
        val mav = new ModelAndView("cart");

        val cartItems = sessionCartService.getCartBooks();
        mav.addObject("items", cartItems);
        val card = loyaltyCardService.getLoyaltyCardByUsername(username);
        val hasCard = card.isPresent();
        mav.addObject("hasCard", hasCard);
        if (hasCard) {
            mav.addObject("cardPoints", card.get().getPoints());
        }
        return mav;
    }
}

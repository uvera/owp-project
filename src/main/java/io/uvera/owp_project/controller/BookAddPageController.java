package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.BookAddDTO;
import io.uvera.owp_project.service.BookService;
import io.uvera.owp_project.service.GenreService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/book/add")
public class BookAddPageController {
    private final GenreService genreService;
    private final BookService bookService;

    public BookAddPageController(GenreService genreService, BookService bookService) {
        this.genreService = genreService;
        this.bookService = bookService;
    }

    @GetMapping()
    public ModelAndView get() {
        val mav = new ModelAndView("book-add");
        mav.addObject("book", new BookAddDTO());
        mav.addObject("genres", genreService.getGenres());
        mav.addObject("hasErrors", false);
        return mav;
    }

    @PostMapping
    public ModelAndView post(@Valid @ModelAttribute BookAddDTO dto, BindingResult result) {
        val mav = new ModelAndView("book-add");
        if (result.hasErrors() || dto.getGenres() == null || dto.getGenres().isEmpty() || !bookService.saveBook(dto)) {
            mav.addObject("book", dto);
            mav.addObject("genres", genreService.getGenres());
            mav.addObject("hasErrors", true);
            return mav;
        }

        mav.setViewName("book-add-success");

        return mav;
    }
}

package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.CartChangeDto;
import io.uvera.owp_project.service.BookService;
import io.uvera.owp_project.service.SessionCartService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/cart")
public class RestCartController {
    private final SessionCartService sessionCartService;
    private final BookService bookService;

    public RestCartController(SessionCartService sessionCartService, BookService bookService) {
        this.sessionCartService = sessionCartService;
        this.bookService = bookService;
    }

    @PostMapping("/{isbn}")
    public ResponseEntity<HttpStatus> addToCart(@PathVariable("isbn") String isbn) {
        if (bookService.getBookByIsbnIfAvailable(isbn).isEmpty())
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        sessionCartService.addToCartByIsbn(isbn);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{isbn}")
    public ResponseEntity<HttpStatus> changeCartItem(
            @Valid @RequestBody CartChangeDto dto, BindingResult result, @PathVariable("isbn") String isbn) {
        if (result.hasErrors())
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return sessionCartService.changeCartItem(isbn, dto.getAmount())
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("{isbn}")
    public ResponseEntity<HttpStatus> deleteCartItem(@PathVariable("isbn") String isbn) {
        return sessionCartService.deleteCartItem(isbn)
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}

package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.PerformShopDto;
import io.uvera.owp_project.service.ShopService;
import io.uvera.owp_project.session.ShoppingCartSession;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/shop")
public class ShopController {
    private final ShopService shopService;

    private final ShoppingCartSession shoppingCartSession;

    public ShopController(ShopService shopService, ShoppingCartSession shoppingCartSession) {
        this.shopService = shopService;
        this.shoppingCartSession = shoppingCartSession;
    }

    @PostMapping
    public ResponseEntity<Object> performShop(@Valid @RequestBody PerformShopDto dto,
                                              BindingResult result, Authentication auth) {
        val userDetails = (UserDetails) auth.getPrincipal();
        if (result.hasErrors())
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (shoppingCartSession.getCart().isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if (!shopService.checkBooksAvailability())
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);

        val username = userDetails.getUsername();
        val didPerform = shopService.executePurchase(dto, username);
        return didPerform ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}

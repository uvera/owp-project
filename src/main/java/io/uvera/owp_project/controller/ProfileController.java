package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.SimpleDtoWithBoolean;
import io.uvera.owp_project.service.LoyaltyCardRequestService;
import io.uvera.owp_project.service.ShopService;
import io.uvera.owp_project.service.UserService;
import io.uvera.owp_project.service.WishListService;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping("/profile")
public class ProfileController {
    private final UserService userService;
    private final LoyaltyCardRequestService loyaltyCardRequestService;
    private final ShopService shopService;
    private final WishListService wishListService;

    public ProfileController(UserService userService, LoyaltyCardRequestService loyaltyCardRequestService, ShopService shopService, WishListService wishListService) {
        this.userService = userService;
        this.loyaltyCardRequestService = loyaltyCardRequestService;
        this.shopService = shopService;
        this.wishListService = wishListService;
    }


    @GetMapping
    public ModelAndView getSelfProfile(Authentication auth) {
        val userDetails = (UserDetails) auth.getPrincipal();
        val username = userDetails.getUsername();
        val mav = new ModelAndView("profile");
        val user = userService.getUserByUsername(username);
        mav.addObject("canRequestLoyaltyCard", loyaltyCardRequestService.canUserRequestLoyaltyCard(username));
        user.ifPresent(u -> mav.addObject("user", u));
        user.ifPresent(u -> mav.addObject("shopped", shopService.getUserShoppingsByUsername(u.getUsername())));
        user.ifPresent(u -> mav.addObject("wishlist", wishListService.getWishListForUsername(u.getUsername())));
        return mav;
    }

    @GetMapping(value = "/{username}")
    public ModelAndView getProfileByUsername(@PathVariable("username") String username) {
        val mav = new ModelAndView("profile");
        val user = userService.getUserByUsername(username);
        user.ifPresent(u -> mav.addObject("user", u));
        user.ifPresent(u -> mav.addObject("shopped", shopService.getUserShoppingsByUsername(u.getUsername())));
        return mav;
    }

    @PostMapping(value = "/{username}/role")
    public ResponseEntity<Object> updateRole(Principal principal,
                                             @PathVariable("username") String username,
                                             @RequestBody SimpleDtoWithBoolean dto) {
        val role = dto.getValue();
        if (principal.getName().equalsIgnoreCase(username))
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();

        val user = userService.getUserByUsername(username);
        if (user.isEmpty()) return ResponseEntity.notFound().build();

        val strRole = role ? "admin" : "user";
        userService.updateRole(username, strRole);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{username}/active")
    public ResponseEntity<Object> updateActive(Principal principal, @PathVariable(
            "username") String username, @ModelAttribute("active") SimpleDtoWithBoolean dto) {
        val status = dto.getValue();
        if (principal.getName().equalsIgnoreCase(username))
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        if (userService.isUserAdmin(username))
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();

        val user = userService.getUserByUsername(username);
        if (user.isEmpty()) return ResponseEntity.notFound().build();

        userService.updateActive(username, status);
        return ResponseEntity.ok().build();
    }

}

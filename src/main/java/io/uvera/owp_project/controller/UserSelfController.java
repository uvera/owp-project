package io.uvera.owp_project.controller;

import io.uvera.owp_project.service.BookService;
import io.uvera.owp_project.service.LoyaltyCardRequestService;
import io.uvera.owp_project.service.WishListService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping("/self")
public class UserSelfController {
    private final LoyaltyCardRequestService loyaltyCardRequestService;
    private final BookService bookService;
    private final WishListService wishListService;

    public UserSelfController(LoyaltyCardRequestService loyaltyCardRequestService, BookService bookService, WishListService wishListService) {
        this.loyaltyCardRequestService = loyaltyCardRequestService;
        this.bookService = bookService;
        this.wishListService = wishListService;
    }

    @PostMapping("/lcr")
    public ModelAndView submit(Principal principal) {
        if (!loyaltyCardRequestService.canUserRequestLoyaltyCard(principal.getName()))
            return new ModelAndView("redirect:/profile?err");
        loyaltyCardRequestService.submitRequestForUsername(principal.getName());
        return new ModelAndView("redirect:/profile?success");
    }

    @PostMapping("/wishlist/{isbn}")
    public ModelAndView submitWishListItem(@PathVariable String isbn, Principal principal) {
        if (bookService.getBookByIsbnIfAvailable(isbn).isEmpty()) {
            return new ModelAndView("redirect:/");
        }
        wishListService.insertWishListItemByIsbnAndUsername(isbn, principal.getName());
        return new ModelAndView("redirect:/profile");
    }

    @DeleteMapping("/wishlist/{isbn}")
    public ResponseEntity<Object> deleteWishListItem(@PathVariable String isbn, Principal principal) {
        wishListService.removeWishListItemForUsernameAndIsbn(principal.getName(), isbn);
        return ResponseEntity.ok().build();
    }
}

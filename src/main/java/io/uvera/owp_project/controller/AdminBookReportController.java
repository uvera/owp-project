package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.SearchReportDTO;
import io.uvera.owp_project.model.BookReport;
import io.uvera.owp_project.service.BookReportService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;

@Controller
@RequestMapping("/admin-panel/book-report")
public class AdminBookReportController {
    private final BookReportService bookReportService;

    public AdminBookReportController(BookReportService bookReportService) {this.bookReportService = bookReportService;}

    @GetMapping
    public ModelAndView get(@ModelAttribute SearchReportDTO dto) {
        val mav = new ModelAndView("admin-panel-report");
        val reports = bookReportService.getAllReportsFromDto(dto);
        mav.addObject("reports", reports);
        mav.addObject("startDate", dto.getStartDate());
        mav.addObject("endDate", dto.getEndDate());

        mav.addObject("allAmountSold", reports.stream()
                .map(BookReport::getAmountSold)
                .mapToInt(BigDecimal::intValue)
                .sum());

        mav.addObject("allTotalPrice", reports.stream()
                .map(BookReport::getTotalPrice)
                .mapToDouble(BigDecimal::doubleValue)
                .sum());

        return mav;

    }
}

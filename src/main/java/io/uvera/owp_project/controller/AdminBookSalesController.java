package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.BookSaleDto;
import io.uvera.owp_project.service.BookSalesService;
import io.uvera.owp_project.service.BookService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin-panel/book-sales")
public class AdminBookSalesController {
    private final BookSalesService bookSalesService;
    private final BookService bookService;

    public AdminBookSalesController(BookSalesService bookSalesService, BookService bookService) {
        this.bookSalesService = bookSalesService;
        this.bookService = bookService;
    }

    @GetMapping
    public ModelAndView get() {
        val mav = new ModelAndView("admin-panel-book-sales");
        mav.addObject("sales", bookSalesService.getBookSales());
        mav.addObject("books", bookService.getBooks());
        return mav;
    }

    @PostMapping
    public ModelAndView post(@Valid @ModelAttribute BookSaleDto dto, BindingResult result) {
        val mav = new ModelAndView("redirect:/admin-panel/book-sales");

        if (result.hasErrors()) {
            mav.setViewName("admin-panel-book-sales");
            mav.addObject("sales", bookSalesService.getBookSales());
            mav.addObject("books", bookService.getBooks());
            mav.addObject("errors", result.getAllErrors());
            return mav;
        }
        bookSalesService.addBookSaleFromDto(dto);

        return mav;
    }
}

package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.SearchBookDTO;
import io.uvera.owp_project.model.Book;
import io.uvera.owp_project.service.BookService;
import io.uvera.owp_project.service.GenreService;
import io.uvera.owp_project.util.OwpUtil;
import lombok.val;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/")
public class MainPageController {
    private final BookService bookService;
    private final GenreService genreService;

    public MainPageController(BookService bookService, GenreService genreService) {
        this.bookService = bookService;
        this.genreService = genreService;
    }

    @GetMapping
    public ModelAndView get(@ModelAttribute SearchBookDTO dto, Authentication auth) {
        val mav = new ModelAndView("index");
        val genres = genreService.getGenres();
        List<Book> books;
        SearchBookDTO search;
        // check if dto is actually querying for something
        if (dto.isQueried()) {
            books = bookService.searchAvailableBooksByDto(dto);
            search = dto;
        }
        // if not, continue as usual
        else {
            if (OwpUtil.hasAuthority(auth, "ADMIN"))
                books = bookService.getBooks();
            else
                books = bookService.getAvailableBooks();
            search = new SearchBookDTO();
        }
        mav.addObject("books", books);
        mav.addObject("search", search);
        mav.addObject("genres", genres);
        return mav;
    }


}

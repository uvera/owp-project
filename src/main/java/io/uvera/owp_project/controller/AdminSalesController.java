package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.SaleDto;
import io.uvera.owp_project.service.SalesService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin-panel/sales")
public class AdminSalesController {
    private final SalesService salesService;

    public AdminSalesController(SalesService salesService) {this.salesService = salesService;}

    @GetMapping
    public ModelAndView get() {
        val mav = new ModelAndView("admin-panel-sales");
        mav.addObject("sales", salesService.getSales());
        return mav;
    }

    @PostMapping
    public ModelAndView post(@Valid @ModelAttribute SaleDto dto, BindingResult result) {
        val mav = new ModelAndView("redirect:/admin-panel/sales");

        if (result.hasErrors()) {
            mav.setViewName("admin-panel-sales");
            mav.addObject("sales", salesService.getSales());
            mav.addObject("errors", result.getAllErrors());
            return mav;
        }
        salesService.addBookSaleFromDto(dto);

        return mav;
    }
}

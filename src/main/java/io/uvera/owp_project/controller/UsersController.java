package io.uvera.owp_project.controller;

import io.uvera.owp_project.service.UserService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/users")
public class UsersController {
    private final UserService userService;

    public UsersController(UserService userService) {this.userService = userService;}

    @GetMapping
    public ModelAndView get() {
        val mav = new ModelAndView("users");
        mav.addObject("users", userService.getAllUsers());
        return mav;
    }
}

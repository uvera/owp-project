package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.BookChangeDTO;
import io.uvera.owp_project.dto.OrderDTO;
import io.uvera.owp_project.service.BookService;
import io.uvera.owp_project.service.GenreService;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/book/edit/")
public class BookEditPageController {
    private final BookService bookService;
    private final GenreService genreService;

    public BookEditPageController(BookService bookService, GenreService genreService) {
        this.bookService = bookService;
        this.genreService = genreService;
    }

    @GetMapping("{isbn}")
    public ModelAndView get(@PathVariable String isbn) {
        val mav = new ModelAndView("book-edit");
        mav.addObject("genres", genreService.getGenres());
        val book = bookService.getBookByIsbn(isbn);
        book.ifPresent(value -> mav.addObject("book", value));

        return mav;
    }

    @PostMapping("{isbn}")
    public ModelAndView post(@PathVariable String isbn, @Valid @ModelAttribute BookChangeDTO dto, BindingResult result) {
        val mav = new ModelAndView("book-edit");
        val book = bookService.getBookByIsbn(isbn);

        if (book.isEmpty())
            return new ModelAndView("redirect:/");

        val bookUnwrap = book.get();

        if (!result.hasErrors()
                && dto.getGenres() != null
                && !dto.getGenres().isEmpty()
                && bookService.updateBook(bookUnwrap, dto)) {
            mav.addObject("success", true);
        } else {
            mav.addObject("hasErrors", true);
        }

        mav.addObject("book", bookService.getBookByIsbn(isbn).orElse(null));
        mav.addObject("genres", genreService.getGenres());

        return mav;


    }

    @PostMapping("/order/{isbn}")
    @ResponseBody
    public ResponseEntity<Object> postOrder(@PathVariable String isbn,
                                            @Valid @RequestBody OrderDTO dto,
                                            BindingResult result) {
        val book = bookService.getBookByIsbn(isbn);
        if (book.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if (result.hasErrors())
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        bookService.orderForBook(book.get(), dto.getValue());

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}

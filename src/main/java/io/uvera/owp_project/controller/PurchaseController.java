package io.uvera.owp_project.controller;

import io.uvera.owp_project.service.ShopService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/purchases")
public class PurchaseController {
    private final ShopService shopService;

    public PurchaseController(ShopService shopService) {this.shopService = shopService;}

    @GetMapping("{id}")
    public ModelAndView get(@PathVariable Long id) {
        val mav = new ModelAndView("purchase");
        val purchase = shopService.getUserShoppingById(id);
        mav.addObject("purchase", purchase);
        return mav;
    }
}

package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.CommentDto;
import io.uvera.owp_project.model.Comment;
import io.uvera.owp_project.service.BookService;
import io.uvera.owp_project.service.CommentService;
import io.uvera.owp_project.util.OwpUtil;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/book/{isbn}")
public class BookPageController {
    private final BookService bookService;
    private final CommentService commentService;

    public BookPageController(BookService bookService, CommentService commentService) {
        this.bookService = bookService;
        this.commentService = commentService;
    }

    @GetMapping
    public ModelAndView get(@PathVariable String isbn, Authentication auth) {
        val mav = new ModelAndView("book");
        if (OwpUtil.hasAuthority(auth, "ADMIN")) {
            val book = bookService.getBookByIsbn(isbn);
            book.ifPresent(value -> mav.addObject("book", value));
        } else {
            val book = bookService.getBookByIsbnIfAvailable(isbn);
            book.ifPresent(value -> mav.addObject("book", value));
        }

        return mav;
    }

    @GetMapping("/comments")
    public ResponseEntity<Object> getBookComments(@PathVariable String isbn, Authentication auth) {
        List<Comment> comments = new ArrayList<>();
        var status = HttpStatus.OK;
        if (OwpUtil.hasAuthority(auth, "ADMIN")) {
            val book = bookService.getBookByIsbn(isbn);
            book.ifPresent(e -> comments.addAll(e.getComments()));
        } else {
            val book = bookService.getBookByIsbnIfAvailable(isbn);
            book.ifPresent(e -> comments.addAll(e.getComments()));
        }
        return new ResponseEntity<>(comments, status);
    }

    @GetMapping("/can-comment")
    public ResponseEntity<Object> getCanComment(@PathVariable String isbn, Principal principal) {
        if (principal == null)
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        if (commentService.canComment(isbn, principal.getName()))
            return ResponseEntity.ok().build();
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/do-comment")
    public ResponseEntity<Object> postDoComment(@PathVariable String isbn, Principal principal,
                                                @Valid @RequestBody CommentDto dto, BindingResult result) {
        val book = bookService.getBookByIsbnIfAvailable(isbn);
        if (book.isEmpty())
            return ResponseEntity.notFound().build();
        if (!commentService.canComment(isbn, principal.getName()))
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        if (result.hasErrors())
            return ResponseEntity.badRequest().build();

        commentService.saveComment(isbn, principal.getName(), dto);

        return ResponseEntity.ok().build();

    }

}

package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.AdminCommentReviewDto;
import io.uvera.owp_project.service.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin-panel/book-comments")
public class AdminBookCommentsController {
    private final CommentService commentService;

    public AdminBookCommentsController(CommentService commentService) {this.commentService = commentService;}

    @GetMapping
    public ModelAndView get() {
        return new ModelAndView("admin-panel-book-comments");
    }

    @GetMapping("/api")
    public ResponseEntity<Object> getComments() {
        return ResponseEntity.ok().body(commentService.getAllCommentsPending());
    }

    @PutMapping("/api")
    public ResponseEntity<Object> reviewComment(@Valid @RequestBody AdminCommentReviewDto dto, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        }
        if (!commentService.canReviewComment(dto)) {
            return ResponseEntity.badRequest().build();
        }

        commentService.updateComment(dto);
        return ResponseEntity.ok().build();
    }
}

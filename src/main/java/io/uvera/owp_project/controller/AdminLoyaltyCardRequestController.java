package io.uvera.owp_project.controller;

import io.uvera.owp_project.dto.AdminLoyaltyCardRequestDTO;
import io.uvera.owp_project.service.LoyaltyCardRequestService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/loyalty-card-requests")
public class AdminLoyaltyCardRequestController {
    private final LoyaltyCardRequestService loyaltyCardRequestService;

    public AdminLoyaltyCardRequestController(LoyaltyCardRequestService loyaltyCardRequestService) {this.loyaltyCardRequestService = loyaltyCardRequestService;}

    @GetMapping
    public ModelAndView get() {
        val mav = new ModelAndView("loyalty-card-requests");
        mav.addObject("userRequests", loyaltyCardRequestService.getAllRequests());
        return mav;
    }

    @PostMapping
    public ModelAndView post(@Valid @ModelAttribute AdminLoyaltyCardRequestDTO dto, BindingResult result) {
        if (result.hasErrors())
            return new ModelAndView("redirect:/loyalty-card-requests?err");
        loyaltyCardRequestService.performUpdateFromDto(dto);
        return new ModelAndView("redirect:/loyalty-card-requests?success");
    }
}

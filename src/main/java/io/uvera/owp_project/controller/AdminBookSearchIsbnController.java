package io.uvera.owp_project.controller;

import io.uvera.owp_project.service.BookService;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin-panel/book-search")
public class AdminBookSearchIsbnController {
    private final BookService bookService;

    public AdminBookSearchIsbnController(BookService bookService) {this.bookService = bookService;}

    @GetMapping
    public ModelAndView get(@RequestParam(value = "isbn", required = false) String isbn) {
        if (isbn != null) {
            val book = bookService.getBookByIsbn(isbn);
            if (book.isEmpty()) {
                return new ModelAndView("redirect:/admin-panel/book-search?err");
            } else {
                return new ModelAndView("redirect:/book/" + book.get().getIsbn());
            }
        }
        return new ModelAndView("admin-panel-search-book");
    }

}

package io.uvera.owp_project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin-panel")
public class AdminPanelController {
    @GetMapping
    public ModelAndView get() {
        return new ModelAndView("admin-panel");
    }

}

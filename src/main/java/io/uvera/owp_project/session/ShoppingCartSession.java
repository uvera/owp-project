package io.uvera.owp_project.session;

import lombok.*;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static org.springframework.web.context.WebApplicationContext.SCOPE_SESSION;

@Component
@Scope(scopeName = SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ShoppingCartSession implements Serializable {
    private final HashSet<Item> items = new HashSet<>();

    public void addToCart(Item item) {
        if (!this.items.contains(item)) {
            this.items.add(item);
        } else {
            val cartItem = this.items.stream().filter(e -> e.getBookIsbn().equals(item.getBookIsbn())).findFirst();
            // should be present always
            if (cartItem.isPresent()) {
                val cartItemUnwrapped = cartItem.get();
                cartItemUnwrapped.addAmount(item.getAmount());
            }
        }
    }

    public void dropCart() {items.clear();}

    public boolean removeFromCart(String isbn) {
        return items.removeIf(e -> e.getBookIsbn().equals(isbn));
    }

    public boolean editCartItem(String isbn, Long amount) {
        val item = this.items.stream().filter(e -> e.getBookIsbn().equals(isbn)).findFirst();
        if (item.isEmpty())
            return false;
        item.get().setAmount(amount);
        return true;
    }

    public Set<Item> getCart() {
        return new HashSet<>(this.items);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class Item implements Serializable {
        private String bookIsbn;
        private Long amount;

        public void addAmount(Long amount) {
            this.amount += amount;
        }
    }
}

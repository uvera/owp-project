package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.Comment;
import lombok.val;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class CommentRepository {
    //language=MySQL
    private static final String CAN_COMMENT_BY_ISBN_AND_NAME = """
                SELECT 1
                FROM Purchase P
                INNER JOIN BoughtBook BB on P.id = BB.purchaseId
                WHERE BB.bookIsbn = :isbn
                AND P.username = :name
                LIMIT 1
            """;
    //language=MySQL
    private static final String ALREADY_COMMENTED = """
            SELECT 1
            FROM Comment
            WHERE bookIsbn = :isbn
            AND author = :name
            AND (status = 'pending'
             OR status = 'accepted')
            LIMIT 1
            """;
    private static final ResultSetExtractor<Boolean> rseCanComment = ResultSet::next;
    //language=MySQL
    private static final String INSERT_COMMENT = """
            REPLACE INTO Comment (text, rating, author, bookIsbn, status, dateOfPosting)
            VALUES (:text, :rating, :author, :bookIsbn, :status, :date)
            """;
    //language=MySQL
    private static final String GET_COMMENTS_TO_REVIEW = """
            SELECT
            C.text as text,
            C.rating as rating,
            C.dateOfPosting as dateOfPosting,
            C.author as author,
            B.name as bookName,
            C.bookIsbn as bookIsbn
            FROM Comment C
            INNER JOIN Book B on C.bookIsbn = B.isbn
            WHERE C.status = 'pending'
            """;
    private static final ResultSetExtractor<List<Comment>> rseCommentsToReview = rs -> {
        val list = new ArrayList<Comment>();
        while (rs.next()) {
            val comment = Comment.builder()
                    .text(rs.getString("text"))
                    .rating(rs.getInt("rating"))
                    .author(rs.getString("author"))
                    .dateOfPosting(rs.getObject("dateOfPosting", LocalDate.class))
                    .bookName(rs.getString("bookName"))
                    .bookIsbn(rs.getString("bookIsbn"))
                    .build();
            list.add(comment);
        }

        return list;
    };
    private static final String FIND_BY_AUTHOR_AND_BOOKISBN_AND_STATUS = """
            SELECT 1 FROM Comment
            WHERE author = :author
            AND bookIsbn = :bookIsbn
            AND status = :status
            """;
    //language=MySQL
    private static final String UPDATE_STATUS_WHERE_ISBN_AND_AUTHOR = """
            UPDATE Comment SET status = :status
            WHERE bookIsbn = :isbn
            AND author = :author
            """;

    private final NamedParameterJdbcTemplate npjt;

    public CommentRepository(NamedParameterJdbcTemplate npjt) {this.npjt = npjt;}

    public Boolean canComment(String isbn, String name) {
        val map = new MapSqlParameterSource()
                .addValue("name", name)
                .addValue("isbn", isbn);
        if (Objects.requireNonNull(npjt.query(ALREADY_COMMENTED, map, rseCanComment))) {
            return false;
        }
        return npjt.query(CAN_COMMENT_BY_ISBN_AND_NAME, map, rseCanComment);
    }

    public void save(Comment c) {
        val map = new MapSqlParameterSource()
                .addValue("text", c.getText())
                .addValue("rating", c.getRating())
                .addValue("author", c.getAuthor())
                .addValue("bookIsbn", c.getBookIsbn())
                .addValue("status", c.getStatus().toString().toLowerCase())
                .addValue("date", c.getDateOfPosting());
        npjt.update(INSERT_COMMENT, map);
    }

    public List<Comment> getAllCommentsToReview() {
        return npjt.query(GET_COMMENTS_TO_REVIEW, rseCommentsToReview);
    }

    public Boolean findByAuthorAndBookIsbnAndStatusIsThere(String author, String bookIsbn,
                                                           Comment.Status status) {
        val map = new MapSqlParameterSource()
                .addValue("author", author)
                .addValue("bookIsbn", bookIsbn)
                .addValue("status", status.toString().toLowerCase());
        return npjt.query(FIND_BY_AUTHOR_AND_BOOKISBN_AND_STATUS, map, ResultSet::next);
    }

    public void updateByAuthorAndBookIsbnWithStatus(String author, String bookIsbn, Comment.Status status) {
        val map = new MapSqlParameterSource()
                .addValue("status", status.toString().toLowerCase())
                .addValue("isbn", bookIsbn)
                .addValue("author", author);
        npjt.update(UPDATE_STATUS_WHERE_ISBN_AND_AUTHOR, map);
    }
}

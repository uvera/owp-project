package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.User;
import lombok.val;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    private static final ResultSetExtractor<User> userRse =
            rs -> rs.next() ?
                    new BeanPropertyRowMapper<>(User.class).mapRow(rs, 1)
                    : null;
    //language=MySQL
    private static final String SQL_FIND_BY_USERNAME = """
                SELECT * FROM User
                WHERE username = :username
            """;
    //language=MySQL
    private static final String SQL_FIND_BY_EMAIL = """
                SELECT * FROM User
                WHERE email = :email
            """;
    //language=MySQL
    private static final String SQL_FIND_ALL = """
            SELECT * FROM User
            """;
    //language=MySQL
    private static final String UPDATE_USER_ROLE_BY_USERNAME = """
            UPDATE User SET role = :role WHERE username = :username
            """;
    //language=MySQL
    private static final String UPDATE_USER_ACTIVE_BY_USERNAME = """
            UPDATE User SET active = :active WHERE username = :username
            """;
    private final NamedParameterJdbcTemplate npjt;

    public UserRepository(NamedParameterJdbcTemplate npjt) {
        this.npjt = npjt;
    }

    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(
                npjt.query(
                        SQL_FIND_BY_USERNAME, new MapSqlParameterSource()
                                .addValue("username", username, Types.VARCHAR),
                        userRse
                )
        );
    }

    public Optional<User> findByEmail(String email) {
        return Optional.ofNullable(
                npjt.query(
                        SQL_FIND_BY_EMAIL, new MapSqlParameterSource()
                                .addValue("email", email, Types.VARCHAR),
                        userRse
                )
        );

    }

    public void save(User user) {
        val insert = new SimpleJdbcInsert(npjt.getJdbcTemplate())
                .withTableName("User");
        insert.execute(user.toMap());
    }

    public List<User> findAll() {
        return npjt.query(SQL_FIND_ALL, new BeanPropertyRowMapper<>(User.class));
    }

    public void updateUserRoleByUsername(String username, String strRole) {
        val map = new MapSqlParameterSource()
                .addValue("role", strRole)
                .addValue("username", username);
        npjt.update(UPDATE_USER_ROLE_BY_USERNAME, map);
    }

    public void updateUserActiveByUsername(String username, boolean active) {
        val map = new MapSqlParameterSource()
                .addValue("active", active)
                .addValue("username", username);
        npjt.update(UPDATE_USER_ACTIVE_BY_USERNAME, map);
    }
}

package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.Genre;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GenreRepository {
    //language=SQL
    private static final String SQL_FIND_ALL = """
            SELECT * FROM
            Genre
            WHERE disabled != true;
            """;
    private final JdbcTemplate jt;

    public GenreRepository(JdbcTemplate jt) {this.jt = jt;}

    public List<Genre> findAll() {
        return jt.query(SQL_FIND_ALL, new BeanPropertyRowMapper<>(Genre.class));
    }
}

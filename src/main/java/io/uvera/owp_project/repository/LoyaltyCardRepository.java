package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.LoyaltyCard;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class LoyaltyCardRepository {
    //language=MySQL
    private static final String SAVE_FOR_USERNAME = """
            INSERT INTO LoyaltyCard (username) VALUES (:username) 
            """;
    private final NamedParameterJdbcTemplate npjt;

    public LoyaltyCardRepository(NamedParameterJdbcTemplate npjt) {this.npjt = npjt;}

    public void save(LoyaltyCard lc) {
        npjt.update(SAVE_FOR_USERNAME, new MapSqlParameterSource().addValue("username", lc.getUsername()));
    }

    public Optional<LoyaltyCard> findByUsername(String username) {
        return Optional.ofNullable(npjt.query("SELECT * FROM LoyaltyCard WHERE username = :username",
                                              new MapSqlParameterSource().addValue(
                                                      "username", username), rs -> rs.next() ? LoyaltyCard.builder()
                        .username(rs.getString("username"))
                        .points(rs.getInt("points"))
                        .build() : null
        ));
    }
}

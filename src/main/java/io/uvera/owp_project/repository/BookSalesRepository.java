package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.BookSale;
import lombok.val;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Map.entry;

@Repository
public class BookSalesRepository {
    //language=MySQL
    private static final String SQL_FIND_ALL = """
            SELECT * FROM BookSale
            """;
    //language=MySQL
    private static final String SQL_FIND_BY_SALE_DATE_AND_BOOK_ISBN = """
            SELECT * FROM BookSale
            WHERE saleDate = :saleDate AND bookIsbn = :bookIsbn 
            """;
    private final NamedParameterJdbcTemplate npjt;

    public BookSalesRepository(NamedParameterJdbcTemplate npjt) {this.npjt = npjt;}

    public List<BookSale> findAll() {
        return npjt.query(SQL_FIND_ALL, rs -> {
            val list = new ArrayList<BookSale>();
            while (rs.next()) {
                val bookSale = BookSale.builder()
                        .bookIsbn(rs.getString("bookIsbn"))
                        .percentDown(rs.getByte("percentDown"))
                        .saleDate(rs.getDate("saleDate").toLocalDate())
                        .build();
                list.add(bookSale);
            }

            return list;
        });
    }

    public Optional<BookSale> findBySaleDateAndBookIsbn(LocalDate saleDate, String bookIsbn) {
        val map = new MapSqlParameterSource()
                .addValue("saleDate", saleDate)
                .addValue("bookIsbn", bookIsbn);
        return Optional.ofNullable(npjt.query(SQL_FIND_BY_SALE_DATE_AND_BOOK_ISBN,
                                              map, rs -> rs.next() ? BookSale
                        .builder()
                        .bookIsbn(rs.getString("bookIsbn"))
                        .percentDown(rs.getByte("percentDown"))
                        .saleDate(rs.getDate("saleDate").toLocalDate())
                        .build() : null
        ));
    }

    public void save(BookSale bookSale) {
        val insert = new SimpleJdbcInsert(npjt.getJdbcTemplate()).withTableName("BookSale");
        insert.execute(Map.ofEntries(
                entry("percentDown", bookSale.getPercentDown()),
                entry("saleDate", bookSale.getSaleDate()),
                entry("bookIsbn", bookSale.getBookIsbn())
        ));
    }
}

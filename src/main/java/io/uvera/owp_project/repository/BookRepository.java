package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.Book;
import io.uvera.owp_project.model.Comment;
import io.uvera.owp_project.model.Genre;
import lombok.val;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class BookRepository {
    //language=SQL
    private static final Function<String, String> addAvailableQuery = str -> str + " WHERE B.availableAmount > 0";
    private static final ResultSetExtractor<Optional<Book>> rseSingle = rs -> {
        Book book = null;

        while (rs.next()) {
            // build the book with Lombok builder
            if (book == null)
                book = Book.builder()
                        .name(rs.getString("name"))
                        .isbn(rs.getString("isbn"))
                        .publishingHouse(rs.getString("publishingHouse"))
                        .yearOfRelease(String.valueOf(rs.getObject("yearOfRelease", LocalDate.class).getYear()))
                        .description(rs.getString("description"))
                        .price(rs.getDouble("price"))
                        .numOfPages(rs.getInt("numOfPages"))
                        .typeOfWrap(Book.TypeOfWrap.valueOf(rs.getString("typeOfWrap").toUpperCase()))
                        .typeOfLetter(Book.TypeOfLetter.valueOf(rs.getString("typeOfLetter").toUpperCase()))
                        .language(rs.getString("language"))
                        .availableAmount(rs.getInt("availableAmount"))
                        .authors(new HashSet<>())
                        .genres(new HashSet<>())
                        .comments(new HashSet<>())
                        .build();
            // for each row, check if there's a sale TODAY
            val saleDate = rs.getObject("saleDate", LocalDate.class);
            if (saleDate != null && LocalDate.now().isEqual(saleDate)) {
                // set book's percent sale
                book.setPercentIntSale(rs.getInt("salePercentDown"));
            }

            /* Author and genre columns cannot be null due to inner joining, we assume every book has at least one
            * genre and author
            * for each row, add author, authors are unique but we're using the Java HashSet implementation just in
            case
            */
            book.addAuthor(rs.getString("bookAuthorName"));

            // for each row, add genre, genres are unique just as authors
            val genre = new Genre();
            genre.setId(rs.getLong("genreId"));
            genre.setName(rs.getString("genreName"));
            genre.setDescription(rs.getString("genreDescription"));
            book.addGenre(genre);

            // for each row, add the comment, comments are unique per username, but could be null
            val commentAuthor = rs.getString("commentAuthor");
            if (commentAuthor != null) {
                val comment = Comment.builder()
                        .author(commentAuthor)
                        .text(rs.getString("commentText"))
                        .rating(rs.getInt("commentRating"))
                        .dateOfPosting(rs.getObject("commentDateOfPosting", LocalDate.class))
                        .build();
                book.addComment(comment);
            }
        }
        return Optional.ofNullable(book);
    };
    private static final ResultSetExtractor<List<Book>> rse = rs -> {
        Map<String, Book> bookMap = new HashMap<>();
        while (rs.next()) {
            // get the book isbn
            String isbn = rs.getString("isbn");
            // check if book already exists
            Book book = bookMap.get(isbn);
            /* if it doesn't exist, create it once as we'll have duplicate
                columns in some rows from the joined query
             */
            if (book == null) {
                // build the book with Lombok builder
                book = Book.builder()
                        .name(rs.getString("name"))
                        .isbn(rs.getString("isbn"))
                        .publishingHouse(rs.getString("publishingHouse"))
                        .yearOfRelease(String.valueOf(rs.getObject("yearOfRelease", LocalDate.class).getYear()))
                        .description(rs.getString("description"))
                        .price(rs.getDouble("price"))
                        .numOfPages(rs.getInt("numOfPages"))
                        .typeOfWrap(Book.TypeOfWrap.valueOf(rs.getString("typeOfWrap").toUpperCase()))
                        .typeOfLetter(Book.TypeOfLetter.valueOf(rs.getString("typeOfLetter").toUpperCase()))
                        .language(rs.getString("language"))
                        .availableAmount(rs.getInt("availableAmount"))
                        .authors(new HashSet<>())
                        .genres(new HashSet<>())
                        .comments(new HashSet<>())
                        .build();
                // insert the book into the map
                bookMap.put(book.getIsbn(), book);
            }
            // for each row, check if there's a sale TODAY
            val saleDate = rs.getObject("saleDate", LocalDate.class);
            if (saleDate != null && LocalDate.now().isEqual(saleDate)) {
                // set book's percent sale
                book.setPercentIntSale(rs.getInt("salePercentDown"));
            }

            /* Author and genre columns cannot be null due to inner joining, we assume every book has at least one
            * genre and author
            * for each row, add author, authors are unique but we're using the Java HashSet implementation just in
            case
            */
            book.addAuthor(rs.getString("bookAuthorName"));

            // for each row, add genre, genres are unique just as authors
            val genre = new Genre();
            genre.setId(rs.getLong("genreId"));
            genre.setName(rs.getString("genreName"));
            genre.setDescription(rs.getString("genreDescription"));
            book.addGenre(genre);

            // for each row, add the comment, comments are unique per username, but could be null
            val commentAuthor = rs.getString("commentAuthor");
            if (commentAuthor != null) {
                val comment = Comment.builder()
                        .author(commentAuthor)
                        .text(rs.getString("commentText"))
                        .rating(rs.getInt("commentRating"))
                        .dateOfPosting(rs.getObject("commentDateOfPosting", LocalDate.class))
                        .build();
                book.addComment(comment);
            }
        }
        return new ArrayList<>(bookMap.values());
    };
    //language=SQL
    private static final String SQL_FIND_ALL = """
            SELECT B.*,
                A.name as bookAuthorName,
                G.id as genreId,
                G.name as genreName,
                G.description as genreDescription,
                CM.text as commentText,
                CM.rating as commentRating,
                CM.dateOfPosting as commentDateOfPosting,
                CM.author as commentAuthor,
                BS.percentDown as salePercentDown,
                BS.saleDate
                       
            FROM Book B
                INNER JOIN Author A 
                    ON B.isbn = A.bookIsbn
               
                INNER JOIN BooksGenres BG
                    ON B.isbn = BG.bookIsbn
               
                INNER JOIN Genre G
                    ON BG.genreId = G.id
                    
                LEFT JOIN BookSale BS 
                    ON BS.bookIsbn = B.isbn
               
                LEFT JOIN Comment CM
                    ON CM.bookIsbn = B.isbn
                        AND CM.status != 'pending'
                        AND CM.status != 'rejected'
                     """;
    private final JdbcTemplate jt;
    private final NamedParameterJdbcTemplate npjt;

    public BookRepository(JdbcTemplate jt, NamedParameterJdbcTemplate npjt) {
        this.jt = jt;
        this.npjt = npjt;
    }

    public List<Book> findAllAvailable() {
        return jt.query(addAvailableQuery.apply(SQL_FIND_ALL), rse);
    }

    public List<Book> findAll() {
        return jt.query(SQL_FIND_ALL, rse);
    }

    public List<Book> findAllAvailableBySearchParameters(String name, List<Long> genres, Double priceMin, Double priceMax,
                                                         String author, String language) {
        // Helper lambda function for LIKE %:value% inserting
        Function<String, String> strLikeFmt = str -> '%' + str + '%';

        String query = addAvailableQuery.apply(SQL_FIND_ALL);
        val params = new MapSqlParameterSource();
        // for each search parameter build the query
        if (name != null && !name.isEmpty()) {
            query += " AND B.name LIKE :name";
            params.addValue("name", strLikeFmt.apply(name), Types.VARCHAR);
        }
        if (genres != null && genres.size() >= 1) {
            query += " AND G.id IN (:genres)";
            params.addValue("genres", genres, Types.INTEGER);
        }
        if (priceMin != null) {
            query += " AND B.price >= :priceMin";
            params.addValue("priceMin", priceMin, Types.DECIMAL);
        }
        if (priceMax != null) {
            query += " AND B.price <= :priceMax";
            params.addValue("priceMax", priceMax, Types.DECIMAL);
        }
        if (author != null && !author.isEmpty()) {
            query += " AND A.name LIKE :author";
            params.addValue("author", strLikeFmt.apply(author), Types.VARCHAR);
        }
        if (language != null && !language.isEmpty()) {
            query += " AND language LIKE :language";
            params.addValue("language", strLikeFmt.apply(language), Types.VARCHAR);
        }

        return npjt.query(query, params, rse);
    }

    public Optional<Book> findAvailableByIsbn(String isbn) {
        String query = addAvailableQuery.apply(SQL_FIND_ALL) + " AND B.isbn = :isbn";
        return npjt.query(query, new MapSqlParameterSource().addValue("isbn", isbn), rseSingle);
    }

    public Optional<Book> findByIsbn(String isbn) {
        String query = SQL_FIND_ALL + " WHERE B.isbn = :isbn";
        return npjt.query(query, new MapSqlParameterSource().addValue("isbn", isbn), rseSingle);
    }

    @Transactional
    public void updateBookWithGenresAndAuthors(Book book, List<Long> genres, List<String> authors) {
        String isbn = book.getIsbn();
        BiFunction<String, Object, Integer> uQF = (fieldName, param) -> jt.update("UPDATE Book SET " + fieldName + " " +
                                                                                          "= ? WHERE " + fieldName + "<> ? AND isbn = ?",
                                                                                  param, param, isbn);

        uQF.apply("name", book.getName());
        uQF.apply("publishingHouse", book.getPublishingHouse());
        uQF.apply("yearOfRelease", book.getYearOfRelease());
        uQF.apply("description", book.getDescription());
        uQF.apply("price", book.getPrice());
        uQF.apply("numOfPages", book.getNumOfPages());
        uQF.apply("typeOfWrap", book.getTypeOfWrap().toString().toLowerCase());
        uQF.apply("typeOfLetter", book.getTypeOfLetter().toString().toLowerCase());
        uQF.apply("language", book.getLanguage());

        jt.batchUpdate("INSERT IGNORE INTO Author (bookIsbn, name) VALUES (?,?)", authorsBPSS(authors, isbn));
        jt.execute("CREATE TEMPORARY TABLE IF NOT EXISTS authors_tmp (name varchar(255) NOT NULL )");
        List<Object[]> authors_tmp = authors.stream().map(e -> new Object[]{e}).collect(Collectors.toList());
        jt.batchUpdate("INSERT INTO authors_tmp VALUES(?)", authors_tmp);
        jt.update("DELETE FROM Author WHERE bookIsbn = ? AND name NOT IN (SELECT name FROM authors_tmp)", isbn);
        jt.update("DELETE FROM authors_tmp");

        jt.batchUpdate("INSERT IGNORE INTO BooksGenres(bookIsbn, genreId) VALUES (?,?)", genresBPSS(genres, isbn));
        jt.execute("CREATE TEMPORARY TABLE IF NOT EXISTS genres_tmp (id int NOT NULL )");
        List<Object[]> genres_tmp = genres.stream().map(e -> new Object[]{e}).collect(Collectors.toList());
        jt.batchUpdate("INSERT INTO genres_tmp VALUES(?)", genres_tmp);
        jt.update("DELETE FROM BooksGenres WHERE bookIsbn = ? AND genreId NOT IN (SELECT id FROM genres_tmp)", isbn);
        jt.update("DELETE FROM genres_tmp");
    }

    private BatchPreparedStatementSetter authorsBPSS(List<String> authors, String isbn) {
        return new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                val author = authors.get(i);
                ps.setString(1, isbn);
                ps.setString(2, author);
            }

            @Override
            public int getBatchSize() {
                return authors.size();
            }
        };
    }

    private BatchPreparedStatementSetter genresBPSS(List<Long> genres, String isbn) {
        return new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                val genre = genres.get(i);
                ps.setString(1, isbn);
                ps.setLong(2, genre);
            }

            @Override
            public int getBatchSize() {
                return genres.size();
            }
        };
    }

    public void updateBooksAmountAvailable(Book book, Integer value) {
        jt.update("UPDATE Book SET availableAmount = availableAmount + ? WHERE isbn = ?", value, book.getIsbn());
    }

    @Transactional
    public void saveBookWithGenresAndAuthors(Book book, List<Long> genres, List<String> authors) {
        val ms = new MapSqlParameterSource();
        ms.addValue("name", book.getName());
        ms.addValue("isbn", book.getIsbn());
        ms.addValue("publishingHouse", book.getPublishingHouse());
        ms.addValue("yearOfRelease", book.getYearOfRelease());
        ms.addValue("description", book.getDescription());
        ms.addValue("price", book.getPrice());
        ms.addValue("numOfPages", book.getNumOfPages());
        ms.addValue("typeOfWrap", book.getTypeOfWrap().toString().toLowerCase());
        ms.addValue("typeOfLetter", book.getTypeOfLetter().toString().toLowerCase());
        ms.addValue("language", book.getLanguage());
        ms.addValue("availableAmount", 0);

        val isbn = book.getIsbn();
        //language=MySQL
        val sql = """
                INSERT INTO Book (name, isbn, publishingHouse, yearOfRelease, description, price, numOfPages, typeOfWrap, typeOfLetter, language, availableAmount)
                VALUES (:name, :isbn, :publishingHouse, :yearOfRelease, :description, :price, :numOfPages, :typeOfWrap, :typeOfLetter, :language, :availableAmount)
                """;
        npjt.update(sql, ms);
        jt.batchUpdate("INSERT INTO Author (bookIsbn, name) VALUES(?,?)", authorsBPSS(authors, isbn));

        jt.batchUpdate("INSERT INTO BooksGenres (bookIsbn, genreId) VALUES (?,?)", genresBPSS(genres, isbn));
    }

    public List<Book> findAllByIsbn(List<String> isbnList) {
        val sql = SQL_FIND_ALL + " WHERE B.isbn IN (:list)";
        val map = new MapSqlParameterSource().addValue("list", isbnList);
        return npjt.query(sql, map, rse);
    }
}

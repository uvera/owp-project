package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.WishListItem;
import lombok.val;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WishListRepository {
    //language=MySQL
    private static final String FIND_FOR_USERNAME = """
            SELECT
            B.isbn as bookIsbn,
            B.name as bookName
             FROM WishList W
            INNER JOIN Book B
            ON B.isbn = W.bookIsbn
            WHERE username = :username
            """;
    //language=MySQL
    private static final String INSERT_BY_ISBN_AND_USERNAME = """
            REPLACE INTO WishList (username, bookIsbn) VALUES (:username, :bookIsbn)
            """;
    //language=MySQL
    private static final String REMOVE_BY_USERNAME_AND_ISBN = """
            DELETE FROM WishList
            WHERE username = :username
            AND bookIsbn = :isbn
            """;
    private final NamedParameterJdbcTemplate npjt;

    public WishListRepository(NamedParameterJdbcTemplate npjt) {this.npjt = npjt;}

    public List<WishListItem> findWishListForUsername(String username) {
        val map = new MapSqlParameterSource().addValue("username", username);
        return npjt.query(FIND_FOR_USERNAME, map, new BeanPropertyRowMapper<>(WishListItem.class));
    }

    public void insertWishListItemByIsbnAndUsername(String isbn, String name) {
        val map = new MapSqlParameterSource()
                .addValue("bookIsbn", isbn)
                .addValue("username", name);
        npjt.update(INSERT_BY_ISBN_AND_USERNAME, map);
    }

    public void remove(String name, String isbn) {
        val map = new MapSqlParameterSource()
                .addValue("username", name)
                .addValue("isbn", isbn);
        npjt.update(REMOVE_BY_USERNAME_AND_ISBN, map);
    }
}

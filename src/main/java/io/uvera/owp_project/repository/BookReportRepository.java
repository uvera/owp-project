package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.BookReport;
import lombok.val;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;

@Repository
public class BookReportRepository {
    //language=MySQL
    private static final String FIND_ALL = """
            SELECT B.isbn                           as bookIsbn,
                   B.name                           as bookName,
                   A.name                           as authorName,
                   SUM(BB.amountOfBooks)            as amountSold,
                   SUM(BB.amountOfBooks * BB.price) as totalPrice
            FROM BoughtBook BB
                     INNER JOIN Book B
                                ON BB.bookIsbn = B.isbn
                     INNER JOIN Author A on B.isbn = A.bookIsbn
            INNER JOIN Purchase P on BB.purchaseId = P.id
            WHERE P.dateOfPayment BETWEEN COALESCE(:start, P.dateOfPayment) AND COALESCE(:end, P.dateOfPayment)
            GROUP BY B.isbn, B.name, A.name
            """;
    private final NamedParameterJdbcTemplate npjt;
    private ResultSetExtractor<List<BookReport>> rse = rs -> {
        val map = new HashMap<String, BookReport>();
        while (rs.next()) {
            val isbn = rs.getString("bookIsbn");
            var bookReport = map.get(isbn);
            if (bookReport != null) {
                bookReport.getAuthors().add(rs.getString("authorName"));
            } else {
                bookReport = BookReport
                        .builder()
                        .bookIsbn(rs.getString("bookIsbn"))
                        .bookName(rs.getString("bookName"))
                        .authors(new HashSet<>(Collections.singleton(rs.getString("authorName"))))
                        .amountSold(rs.getBigDecimal("amountSold"))
                        .totalPrice(rs.getBigDecimal("totalPrice"))
                        .build();
                map.put(bookReport.getBookIsbn(), bookReport);
            }
        }
        return new ArrayList<>(map.values());
    };

    public BookReportRepository(NamedParameterJdbcTemplate npjt) {this.npjt = npjt;}

    public List<BookReport> findAllByStartDateAndEndDate(LocalDate startDate, LocalDate endDate) {
        val map = new MapSqlParameterSource()
                .addValue("start", startDate)
                .addValue("end", endDate);
        return npjt.query(FIND_ALL, map, rse);
    }
}

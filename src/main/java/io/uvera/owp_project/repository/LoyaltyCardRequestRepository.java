package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.LoyaltyCardRequest;
import io.uvera.owp_project.model.User;
import lombok.val;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class LoyaltyCardRequestRepository {
    //language=SQL
    private static final String SQL_FIND_ALL = """
            SELECT lcr.*, u.firstName as firstName, u.lastName as lastName FROM LoyaltyCardRequest lcr
            INNER JOIN User u
            ON u.username = lcr.username
            ORDER BY lcr.status DESC
            """;
    //language=MySQL
    private static final String SQL_FIND_BY_USERNAME_AND_STATUS_EQ_PENDING_OR_ACCEPTED = """
            SELECT lcr.*, u.firstName as firstName, u.lastName as lastName FROM LoyaltyCardRequest lcr
            INNER JOIN User u
            ON u.username = lcr.username
            WHERE u.username = :username
            AND lcr.status <> 'rejected'
            """;
    //language=MySQL
    private final static String SQL_SAVE_REQUEST = """
                    REPLACE INTO LoyaltyCardRequest (username, status) VALUES (:username, :status)
            """;
    //language=MySQL
    private static final String SQL_FIND_BY_USERNAME = """
            SELECT * FROM LoyaltyCardRequest
            WHERE username = :username
            """;
    private final NamedParameterJdbcTemplate npjt;

    public LoyaltyCardRequestRepository(NamedParameterJdbcTemplate npjt) {this.npjt = npjt;}

    public List<LoyaltyCardRequest> findAll() {
        return npjt.query(SQL_FIND_ALL, rs -> {
            val ret = new ArrayList<LoyaltyCardRequest>();
            while (rs.next()) {
                val lcr = LoyaltyCardRequest.builder()
                        .user(User.builder()
                                      .username(rs.getString("username"))
                                      .firstName(rs.getString("firstName"))
                                      .lastName(rs.getString("lastName"))
                                      .build())
                        .status(LoyaltyCardRequest.Status.valueOf(rs.getString("status").toUpperCase()))
                        .build();
                ret.add(lcr);
            }
            return ret;
        });
    }

    public Optional<LoyaltyCardRequest> findByUsernameAndStatus_PendingOrAccepted(String username) {
        val map = new MapSqlParameterSource().addValue("username", username);
        return Optional.ofNullable(npjt.query(
                SQL_FIND_BY_USERNAME_AND_STATUS_EQ_PENDING_OR_ACCEPTED,
                map,
                rs -> {
                    if (rs.next()) {
                        return LoyaltyCardRequest.builder()
                                .user(User.builder()
                                              .username(rs.getString("username"))
                                              .firstName(rs.getString("firstName"))
                                              .lastName(rs.getString("lastName"))
                                              .build())
                                .status(LoyaltyCardRequest.Status.valueOf(rs.getString("status").toUpperCase()))
                                .build();
                    } else return null;
                }
        ));
    }

    public void save(LoyaltyCardRequest req) {
        val ms = new MapSqlParameterSource()
                .addValue("username", req.getUser().getUsername())
                .addValue("status", req.getStatus().toString());
        npjt.update(SQL_SAVE_REQUEST, ms);
    }

    public Optional<LoyaltyCardRequest> findByUsername(String obj) {
        val map = new MapSqlParameterSource().addValue("username", obj);
        return Optional.ofNullable(npjt.query(
                SQL_FIND_BY_USERNAME,
                map,
                rs -> {
                    if (rs.next()) {
                        return LoyaltyCardRequest.builder()
                                .user(User.builder()
                                              .username(rs.getString("username"))
                                              .build())
                                .status(LoyaltyCardRequest.Status.valueOf(rs.getString("status").toUpperCase()))
                                .build();
                    } else return null;
                }
        ));
    }
}

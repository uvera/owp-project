package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.Sale;
import lombok.val;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Map.entry;

@Repository
public class SalesRepository {
    //language=MySQL
    private static final String SQL_FIND_ALL = """
            SELECT * FROM Sale 
            """;
    //language=MySQL
    private static final String SQL_FIND_BY_SALE_DATE = """
            SELECT * FROM Sale 
            WHERE saleDate = :saleDate
            """;
    private final NamedParameterJdbcTemplate npjt;

    public SalesRepository(NamedParameterJdbcTemplate npjt) {this.npjt = npjt;}

    public List<Sale> findAll() {
        return npjt.query(SQL_FIND_ALL, rs -> {
            val list = new ArrayList<Sale>();
            while (rs.next()) {
                val sale = Sale.builder()
                        .percentDown(rs.getByte("percentDown"))
                        .saleDate(rs.getDate("saleDate").toLocalDate())
                        .build();
                list.add(sale);
            }

            return list;
        });
    }

    public void save(Sale sale) {
        val insert = new SimpleJdbcInsert(npjt.getJdbcTemplate()).withTableName("Sale");
        insert.execute(Map.ofEntries(
                entry("percentDown", sale.getPercentDown()),
                entry("saleDate", sale.getSaleDate())
        ));
    }

    public Optional<Sale> findSaleToday() {
        return findBySaleDate(LocalDate.now());
    }

    public Optional<Sale> findBySaleDate(LocalDate saleDate) {
        val map = new MapSqlParameterSource()
                .addValue("saleDate", saleDate);
        return Optional.ofNullable(npjt.query(SQL_FIND_BY_SALE_DATE,
                                              map, rs -> rs.next() ? Sale
                        .builder()
                        .percentDown(rs.getByte("percentDown"))
                        .saleDate(rs.getDate("saleDate").toLocalDate())
                        .build() : null
        ));
    }
}

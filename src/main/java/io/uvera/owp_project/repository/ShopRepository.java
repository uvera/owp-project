package io.uvera.owp_project.repository;

import io.uvera.owp_project.model.BoughtBook;
import io.uvera.owp_project.service.UserShopping;
import lombok.val;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Repository
public class ShopRepository {
    //language=MySQL
    private static final String INSERT_BOUGHT_BOOK = """
            INSERT INTO BoughtBook (bookIsbn, amountOfBooks, price, purchaseId)
            VALUES (?,?,?,?)
            """;
    //language=MySQL
    private static final String UPDATE_LOYALTY_CARD = """
            UPDATE LoyaltyCard SET points = points - :amount WHERE username = :username
            """;
    //language=MySQL
    private static final String INCREMENT_LOYALTY_CARD = """
                        UPDATE LoyaltyCard SET points = points + :amount WHERE username = :username
            """;
    //language=MySQL
    private static final String FIND_USER_SHOPPINGS_BY_USERNAME = """
            SELECT P.id             as purchaseId,
                   P.dateOfPayment  as dateOfPayment,
                   P.username       as username,
                   BB.amountOfBooks as amountOfbooksBought,
                   BB.price         as pricePerBoughtBook,
                   B.name           as bookName
            FROM Purchase P
                     INNER JOIN BoughtBook BB
                                ON BB.purchaseId = P.id
                     INNER JOIN Book B
                                ON BB.bookIsbn = B.isbn
                                WHERE P.username = :username
                        """;
    //language=MySQL
    private static final String FIND_USER_SHOPPINGS_BY_ID = """
            SELECT P.id             as purchaseId,
                   P.dateOfPayment  as dateOfPayment,
                   P.username       as username,
                   BB.amountOfBooks as amountOfbooksBought,
                   BB.price         as pricePerBoughtBook,
                   B.name           as bookName
            FROM Purchase P
                     INNER JOIN BoughtBook BB
                                ON BB.purchaseId = P.id
                     INNER JOIN Book B
                                ON BB.bookIsbn = B.isbn
                                WHERE P.id = :id
                        """;
    //language=MySQL
    private static final String UPDATE_AMOUNT = """
            UPDATE Book SET availableAmount = availableAmount - ?
            WHERE isbn = ?
            """;
    private final NamedParameterJdbcTemplate npjt;
    private final ResultSetExtractor<List<UserShopping>> rseUserShoppings = rs -> {
        HashMap<Long, UserShopping> set = new HashMap<>();
        while (rs.next()) {
            val id = rs.getLong("purchaseId");
            var shopping = set.get(id);
            if (shopping != null) {
                shopping.getBookSet().add(rs.getString("bookName"));
                shopping.setAmountOfBooksBought(shopping.getAmountOfBooksBought() + rs.getLong("amountOfBooksBought"));
                shopping.setTotalPrice(shopping.getTotalPrice().add
                        (rs.getBigDecimal("amountOfBooksBought")
                                 .multiply(rs.getBigDecimal("pricePerBoughtBook"))));
            } else {
                shopping = UserShopping.builder()
                        .purchaseId(rs.getLong("purchaseId"))
                        .dateOfPayment(rs.getObject("dateOfPayment", LocalDate.class))
                        .username(rs.getString("username"))
                        .bookSet(new HashSet<>())
                        .amountOfBooksBought(rs.getLong("amountOfBooksBought"))
                        .totalPrice(rs.getBigDecimal("amountOfBooksBought").multiply(rs.getBigDecimal(
                                "pricePerBoughtBook")))
                        .build();
                shopping.getBookSet().add(rs.getString("bookName"));
                set.put(shopping.getPurchaseId(), shopping);
            }
        }
        return new ArrayList<>(set.values());
    };
    private final ResultSetExtractor<UserShopping> rseUserShopping = rs -> {
        UserShopping shopping = null;
        while (rs.next()) {
            if (shopping != null) {
                shopping.getBookSet().add(rs.getString("bookName"));
                shopping.setTotalPrice(shopping.getTotalPrice().add
                        (rs.getBigDecimal("amountOfBooksBought")
                                 .multiply(rs.getBigDecimal("pricePerBoughtBook"))));
                shopping.setAmountOfBooksBought(shopping.getAmountOfBooksBought() + rs.getLong("amountOfBooksBought"));
            } else {
                shopping = UserShopping.builder()
                        .purchaseId(rs.getLong("purchaseId"))
                        .dateOfPayment(rs.getObject("dateOfPayment", LocalDate.class))
                        .username(rs.getString("username"))
                        .bookSet(new HashSet<>())
                        .amountOfBooksBought(rs.getLong("amountOfBooksBought"))
                        .totalPrice(rs.getBigDecimal("amountOfBooksBought").multiply(rs.getBigDecimal(
                                "pricePerBoughtBook")))
                        .build();
                shopping.getBookSet().add(rs.getString("bookName"));
            }
        }
        return shopping;
    };

    public ShopRepository(NamedParameterJdbcTemplate npjt) {this.npjt = npjt;}

    @Transactional
    public void performPurchase(List<BoughtBook> boughtBooks, String username, Byte useAmountOfPoints) {
        npjt.update(UPDATE_LOYALTY_CARD, new MapSqlParameterSource()
                .addValue("username", username)
                .addValue("amount", useAmountOfPoints));
        performPurchase(boughtBooks, username);
    }

    @Transactional
    public void performPurchase(List<BoughtBook> items, String username) {
        val purchaseId = new SimpleJdbcInsert(npjt.getJdbcTemplate()).withTableName("Purchase")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource().addValue("username", username));

        npjt.getJdbcTemplate().batchUpdate(INSERT_BOUGHT_BOOK, preparedStatement(items, purchaseId));

        val amountToAdd = Math.round(items.stream()
                                             .mapToDouble(e -> e.getPrice() * e.getAmountOfBooks())
                                             .sum() / 1000);
        npjt.update(INCREMENT_LOYALTY_CARD, new MapSqlParameterSource()
                .addValue("username", username)
                .addValue("amount", amountToAdd));
        npjt.getJdbcTemplate().batchUpdate(UPDATE_AMOUNT, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                val boughtBook = items.get(i);
                ps.setLong(1, boughtBook.getAmountOfBooks());
                ps.setString(2, boughtBook.getBookIsbn());
            }

            @Override
            public int getBatchSize() {
                return items.size();
            }
        });
    }

    private BatchPreparedStatementSetter preparedStatement(List<BoughtBook> items, Number purchaseId) {
        return new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                val boughtBook = items.get(i);
                ps.setString(1, boughtBook.getBookIsbn());
                ps.setInt(2, Math.toIntExact(boughtBook.getAmountOfBooks()));
                ps.setDouble(3, boughtBook.getPrice());
                ps.setObject(4, purchaseId);
            }

            @Override
            public int getBatchSize() {
                return items.size();
            }
        };
    }

    public List<UserShopping> findUserShoppingsByUsername(String username) {
        val map = new MapSqlParameterSource().addValue("username", username);
        return npjt.query(FIND_USER_SHOPPINGS_BY_USERNAME, map, rseUserShoppings);
    }

    public UserShopping findUserShoppingById(Long id) {
        val map = new MapSqlParameterSource().addValue("id", id);
        return npjt.query(FIND_USER_SHOPPINGS_BY_ID, map, rseUserShopping);
    }
}

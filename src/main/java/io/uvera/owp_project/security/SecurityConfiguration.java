package io.uvera.owp_project.security;

import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;

    public SecurityConfiguration(@Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService) {this.userDetailsService = userDetailsService;}

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        val auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(userDetailsService);
        auth.setPasswordEncoder(passwordEncoder());
        return auth;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/register/**").not().authenticated()
                .antMatchers("/book/edit/**").hasAuthority("ADMIN")
                .antMatchers("/book/add/**").hasAuthority("ADMIN")
                .antMatchers("/admin-panel/**").hasAuthority("ADMIN")
                .antMatchers("/profile").authenticated()
                .antMatchers("/profile/**").hasAuthority("ADMIN")
                .antMatchers("/users/**").hasAuthority("ADMIN")
                .antMatchers("/loyalty-card-requests/**").hasAuthority("ADMIN")
                .antMatchers("/self/**").hasAuthority("USER")
                .antMatchers("/api/cart/**").hasAuthority("USER")
                .antMatchers("/api/shop/**").hasAuthority("USER")
                .antMatchers("/cart/**").hasAuthority("USER")
                .antMatchers("/purchases/**").authenticated()
                .antMatchers("/book/*/do-comment").hasAuthority("USER")
                .antMatchers("/**").permitAll()

                .and()
                .formLogin()
                .defaultSuccessUrl("/")
                .loginPage("/login")
                .loginProcessingUrl("/login")
                .failureUrl("/login?failure")
                .permitAll()

                .and()
                .logout()
                .logoutSuccessUrl("/")
                .invalidateHttpSession(false)
                .permitAll();
    }
}

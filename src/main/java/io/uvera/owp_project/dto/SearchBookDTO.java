package io.uvera.owp_project.dto;

import lombok.Data;

import java.util.List;

@Data
public class SearchBookDTO {
    private String name;
    private List<Long> genres;
    private Double priceMin;
    private Double priceMax;
    private String author;
    private String language;

    public boolean isQueried() {
        var ret = false;
        if (name != null && !name.isEmpty())
            ret = true;

        if (genres != null && !genres.isEmpty())
            ret = true;

        if (priceMin != null || priceMax != null)
            ret = true;

        if (author != null && !author.isEmpty())
            ret = true;

        if (language != null && !language.isEmpty())
            ret = true;

        return ret;
    }
}

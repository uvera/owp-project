package io.uvera.owp_project.dto;

public class SimpleDtoWithBoolean {
    private boolean value;

    public boolean getValue() {
        return this.value;
    }
}

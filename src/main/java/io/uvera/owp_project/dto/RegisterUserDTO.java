package io.uvera.owp_project.dto;

import io.uvera.owp_project.constraints.annotation.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@FieldsShouldMatch.Series({
        @FieldsShouldMatch(fields = {"password", "passwordAgain"},
                message = "{password.match}"),
        @FieldsShouldMatch(fields = {"email", "emailAgain"},
                message = "{email.match}")

})
public class RegisterUserDTO {
    @NotBlank(message = "{field.username.NotBlank}")
    @UniqueUsername(message = "{field.username.UniqueUsername}")
    private String username;

    @NotBlank(message = "{field.password.NotBlank}")
    private String password;

    @NotBlank(message = "{field.passwordAgain.NotBlank}")
    private String passwordAgain;

    @NotBlank(message = "{field.email.NotBlank}")
    @ValidEmail(message = "{field.email.ValidEmail}")
    @UniqueEmail(message = "{field.email.UniqueEmail}")
    private String email;

    @NotBlank(message = "{field.emailAgain.NotBlank}")
    private String emailAgain;

    @NotBlank(message = "{field.firstName.NotBlank}")
    private String firstName;

    @NotBlank(message = "{field.lastName.NotBlank}")
    private String lastName;

    @NotNull(message = "{field.dateOfBirth.NotNull}")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @LegitDate(message = "{field.dateOfBirth.LegitDate}")
    private LocalDate dateOfBirth;

    @NotBlank(message = "{field.address.NotBlank}")
    private String address;

    @NotBlank(message = "{field.phoneNumber.NotBlank}")
    @ValidNumber(message = "{field.phoneNumber.validNumber}")
    private String phoneNumber;
}

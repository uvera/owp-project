package io.uvera.owp_project.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AdminCommentReviewDto {
    @NotNull
    @NotBlank
    private String author;

    @NotNull
    @NotBlank
    private String bookIsbn;

    @NotNull
    private Boolean accept;
}

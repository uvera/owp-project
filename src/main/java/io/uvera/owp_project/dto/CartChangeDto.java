package io.uvera.owp_project.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class CartChangeDto {
    @NotNull
    @Min(1)
    @Positive
    private long amount;
}

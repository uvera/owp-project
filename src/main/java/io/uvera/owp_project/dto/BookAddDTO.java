package io.uvera.owp_project.dto;

import io.uvera.owp_project.constraints.annotation.UniqueIsbn;
import io.uvera.owp_project.model.Book;
import io.uvera.owp_project.util.OwpConstants;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.util.List;

@Data
public class BookAddDTO {
    @Size(max = 255)
    @NotBlank
    private String name;

    @UniqueIsbn
    @Size(min = 13, max = 13)
    @NotBlank
    private String isbn;

    @Size(max = 255)
    @NotBlank
    private String publishingHouse;

    @Size(max = 1024)
    @NotBlank
    private String authors;

    @NotNull
    @Range(min = 1901, max = OwpConstants.YEAR)
    private Short yearOfRelease;

    @Size(max = 600)
    @NotBlank
    private String description;

    @NotNull
    @DecimalMin("1.0")
    private Double price;

    @Min(1)
    private Integer numOfPages;

    @NotNull
    private Book.TypeOfWrap typeOfWrap;

    @NotNull
    private Book.TypeOfLetter typeOfLetter;

    @Size(max = 255)
    @NotBlank
    private String language;

    private List<Long> genres;

    @NotNull
    private MultipartFile imageFile;
}

package io.uvera.owp_project.dto;

import io.uvera.owp_project.constraints.annotation.LCRAcceptedAlready;
import io.uvera.owp_project.constraints.annotation.LCRRejected;
import io.uvera.owp_project.constraints.annotation.LCRUsernameExists;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AdminLoyaltyCardRequestDTO {
    @LCRUsernameExists
    @LCRAcceptedAlready
    @LCRRejected
    @NotNull
    @NotBlank
    private String username;
    @NotNull
    private Action action;

    public enum Action {
        ACCEPT, DENY
    }
}

package io.uvera.owp_project.dto;

import io.uvera.owp_project.constraints.annotation.BookExistsByIsbn;
import io.uvera.owp_project.constraints.annotation.BookSaleDtoExists;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@BookSaleDtoExists
public class BookSaleDto {
    @Max(value = 99, message = "{bookSaleDto.Max}")
    @Min(value = 1, message = "{bookSaleDto.Min}")
    @NotNull(message = "{bookSaleDto.percentDown.NotNull}")
    private Byte percentDown;

    @FutureOrPresent(message = "{bookSaleDto.Future}")
    @NotNull(message = "{bookSaleDto.date.NotNull}")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate saleDate;

    @NotNull(message = "{bookIsbn.NotNull}")
    @BookExistsByIsbn
    private String bookIsbn;
}

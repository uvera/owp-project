package io.uvera.owp_project.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class CommentDto {
    @Min(1)
    @Max(5)
    @NotNull
    private Integer rating;

    @Size(min = 1, max = 500)
    @NotBlank
    @NotNull
    private String text;
}

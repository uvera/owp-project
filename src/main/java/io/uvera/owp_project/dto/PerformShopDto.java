package io.uvera.owp_project.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class PerformShopDto {
    @Min(0)
    @Max(10)
    @NotNull
    private Byte useAmountOfPoints;
}

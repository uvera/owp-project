package io.uvera.owp_project.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class OrderDTO {
    @Min(1)
    @Max(500)
    @NotNull
    private Integer value;

    public Integer getValue() {
        return value;
    }
}

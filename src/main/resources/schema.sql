-- DROP TABLES THAT HAVE FOREIGN KEYS
DROP TABLE IF EXISTS Author^;
DROP TABLE IF EXISTS BooksGenres^;
DROP TABLE IF EXISTS BoughtBook^;
DROP TABLE IF EXISTS Purchase^;
DROP TABLE IF EXISTS Comment^;
DROP TABLE IF EXISTS LoyaltyCard^;
DROP TABLE IF EXISTS LoyaltyCardRequest^;
DROP TABLE IF EXISTS BookSale^;
DROP TABLE IF EXISTS WishList^;
-- DROP OTHER TABLES
DROP TABLE IF EXISTS Book^;
DROP TABLE IF EXISTS Genre^;
DROP TABLE IF EXISTS User ^;
DROP TABLE IF EXISTS Sale^;
-- DROP PROCEDURES/FUNCTIONS
DROP FUNCTION IF EXISTS GetGenreIdByName^;


CREATE TABLE Book
(
    name            nvarchar(255) NOT NULL,
    isbn            char(13)  NOT NULL PRIMARY KEY,
    publishingHouse nvarchar(255) NOT NULL,
    yearOfRelease YEAR NOT NULL,
    description     text      NOT NULL,
    price           float(3)  NOT NULL,
    numOfPages      mediumint NOT NULL,
    typeOfWrap      enum('soft','hard') NOT NULL,
    typeOfLetter    enum('latin','cyrillic') NOT NULL,
    language        nvarchar(255) NOT NULL,
    availableAmount mediumint NOT NULL
) ^;

CREATE TABLE Author
(
    bookIsbn char(13) NOT NULL,
    name     nvarchar(255) NOT NULL,
    PRIMARY KEY (bookIsbn, name),
    FOREIGN KEY (bookIsbn) REFERENCES Book (isbn)
) ^;

CREATE TABLE Genre
(
    id          int     NOT NULL PRIMARY KEY auto_increment,
    name        nvarchar(30) NOT NULL UNIQUE,
    description text    NOT NULL,
    disabled    boolean NOT NULL DEFAULT FALSE
) ^;

CREATE FUNCTION GetGenreIdByName(paramName nvarchar(30))
    RETURNS int
    DETERMINISTIC
    RETURN (
        SELECT id
        FROM Genre
        WHERE name = paramName)
        ^;

CREATE TABLE BooksGenres
(
    bookIsbn char(13) NOT NULL,
    genreId  int      NOT NULL,
    FOREIGN KEY (bookIsbn) REFERENCES Book (isbn),
    FOREIGN KEY (genreId) REFERENCES Genre (id) ON DELETE NO ACTION
) ^;

CREATE TABLE User
(
    username           nvarchar(30) NOT NULL,
    password           nvarchar(128) NOT NULL,
    email              nvarchar(100) NOT NULL,
    firstName          nvarchar(255) NOT NULL,
    lastName           nvarchar(255) NOT NULL,
    dateOfBirth        date     NOT NULL,
    address            nvarchar(255) NOT NULL,
    phoneNumber        nvarchar(20) NOT NULL,
    dateOfRegistration datetime NOT NULL DEFAULT NOW(),
    role               enum('user','admin') DEFAULT 'user' NOT NULL,
    active             boolean  NOT NULL DEFAULT TRUE,
    PRIMARY KEY (username, email)
) ^;

CREATE TABLE Purchase
(
    id            int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    dateOfPayment date,
    username      nvarchar(30) NOT NULL,
    FOREIGN KEY (username) REFERENCES User (username)
) ^;

CREATE TABLE BoughtBook
(
    bookIsbn      char(13)  NOT NULL,
    amountOfBooks mediumint NOT NULL,
    price         decimal   NOT NULL,
    purchaseId    int       NOT NULL,
    FOREIGN KEY (bookIsbn) REFERENCES Book (isbn),
    FOREIGN KEY (purchaseId) REFERENCES Purchase (id)
) ^;

CREATE TRIGGER `PurchaseSetDateOfPayment`
    BEFORE INSERT
    ON Purchase
    FOR EACH ROW
BEGIN
    IF NEW.dateOfPayment IS NULL THEN
        SET NEW.dateOfPayment = CURDATE();
END IF;
END;
^;

CREATE TABLE Comment
(
    text          text     NOT NULL,
    rating        tinyint(1) NOT NULL CHECK (rating BETWEEN 1 AND 5),
    dateOfPosting date,
    author        nvarchar(30) NOT NULL,
    bookIsbn      char(13) NOT NULL,
    status        enum('pending','accepted','rejected') NOT NULL,
    FOREIGN KEY (author) REFERENCES User (username),
    FOREIGN KEY (bookIsbn) REFERENCES Book (isbn),
    PRIMARY KEY (author, bookIsbn)
) ^;

CREATE TRIGGER `CommentSetDateOfPosting`
    BEFORE INSERT
    ON Comment
    FOR EACH ROW
BEGIN
    IF NEW.dateOfPosting IS NULL THEN
        SET NEW.dateOfPosting = CURDATE();
END IF;
END;
^;

CREATE TABLE LoyaltyCard
(
    username nvarchar(30) NOT NULL,
    points   int(1) NOT NULL DEFAULT 4 CHECK (points > -1),
    PRIMARY KEY (username),
    FOREIGN KEY (username) REFERENCES User (username)
) ^;

CREATE TABLE LoyaltyCardRequest
(
    username nvarchar(30) NOT NULL,
    status   enum('pending', 'accepted', 'rejected') DEFAULT 'pending' NOT NULL,
    PRIMARY KEY (username),
    FOREIGN KEY (username) REFERENCES User (username)
) ^;


CREATE TABLE Sale
(
    percentDown tinyint(1) NOT NULL DEFAULT 20
        CHECK (percentDown BETWEEN 1 AND 100),
    saleDate    date NOT NULL DEFAULT '1970-01-01',
    PRIMARY KEY (saleDate)

) ^;

CREATE TABLE BookSale
(
    percentDown tinyint(1) NOT NULL DEFAULT 20
        CHECK (percentDown BETWEEN 1 AND 100),
    saleDate    date     NOT NULL DEFAULT '1970-01-1',
    bookIsbn    char(13) NOT NULL,
    PRIMARY KEY (saleDate, bookIsbn),
    FOREIGN KEY (bookIsbn) REFERENCES Book (isbn)
)
    ^;

CREATE TABLE WishList
(
    username nvarchar(30) NOT NULL,
    bookIsbn char(13) NOT NULL,
    PRIMARY KEY (username, bookIsbn),
    FOREIGN KEY (username) REFERENCES User (username),
    FOREIGN KEY (bookIsbn) REFERENCES Book (isbn)
) ^;
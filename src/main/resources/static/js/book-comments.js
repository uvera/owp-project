import {h as html, htm, render, useEffect, useState} from './preact.bundle.js';

const h = htm.bind(html);
const csrf = document.querySelector('meta[id=_csrf]').content;

const isbn = document.querySelector("#comments-preact").dataset.isbn

function qsLang(elem) {
    return document.querySelector(`#lang-comment-${elem}`).textContent
}

const localization = {
    sectionTitle: qsLang('section-title'),
    author: qsLang('author'),
    date: qsLang('date'),
    rating: qsLang('rating'),
    text: qsLang('text'),
    submit: qsLang('submit'),
    sweetAlert: qsLang('sweetalert'),
}

document.querySelector("#localization-container").remove()

const commentStateProps = {
    author: '',
    text: '',
    rating: '',
    dateOfPosting: '',
}


function App() {
    const [comments, setComments] = useState([])
    const [canComment, setCanComment] = useState(false)

    useEffect(() => {
        fetch(`/book/${isbn}/comments`, {
            method: 'GET',
        })
            .then(r => r.json())
            .then(r => setComments(r))
            .catch(e => console.log(e))

        fetch(`/book/${isbn}/can-comment`, {
            method: 'GET',
            redirect: 'error',
        })
            .then(r => setCanComment(r.status === 200))
            .catch(e => console.log(e))

    }, [])

    return h`
        <${Comments} comments=${comments}/>
        <${CommentInput} capable=${canComment}/>
        `
}

function Comments({comments}) {
    if (comments.length >= 1)
        return h`
            <h1 class="comment-title">${localization.sectionTitle}</h1>
            <div class="comments">
                ${comments.map(e => h`<${Comment} state=${e}/>`)}
            </div>
        `
}

function Comment({state = commentStateProps}) {
    return h`
    <div class="comment">
        <div class="comment-header">
            <div class="comment-header-item">
                <span class="comment-author-label">${localization.author}</span>
                <span class="comment-author">${state.author}</span>
            </div>
            <div class="comment-header-item">
                <span class="comment-date-label">${localization.date}</span>
                <span class="comment-date">${state.dateOfPosting}</span>
            </div>
            <div class="comment-header-item">
                <span class="comment-rating-label">${localization.rating}</span>
                <span class="comment-rating">${state.rating}</span>
            </div>
        </div>
        <div class="comment-text">
            <p>${state.text}</p>
        </div>
    </div>
    `
}

function CommentInput({capable}) {
    const [commentText, setCommentText] = useState('')
    const [rating, setRating] = useState(1)
    const [canSubmit, setCanSubmit] = useState(false)
    const [notSubmitted, setNotSubmitted] = useState(true)

    function addComment() {
        fetch(`/book/${isbn}/do-comment`, {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': csrf,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                text: commentText,
                rating,
            })
        })
            .then(r => setNotSubmitted(r.status !== 200))
            .then(() => swal(localization.sweetAlert))
    }

    function onChangeCommentText(event) {
        let val = event.target.value
        if (val.length >= 1 && val.length <= 500) {
            setCommentText(val)
        } else {
            val = val.slice(0, 500)
            setCommentText(val)
        }

        setCanSubmit(val.length >= 1 && val.length <= 500)
    }

    if (capable && notSubmitted)
        return h`
        <div class="comment-input">
            <label for="textarea-comment-text">${localization.text}</label>
            <textarea id="textarea-comment-text" rows="4" cols="20" value=${commentText} onchange=${onChangeCommentText}>
            </textarea>
            
            <label for="select-comment-rating">${localization.rating}</label>
            <select name="rating" id="select-comment-rating" value=${rating} onchange=${e => setRating(e.target.value)}>
                <option value="1">★</option>
                <option value="2">★★</option>
                <option value="3">★★★</option>
                <option value="4">★★★★</option>
                <option value="5">★★★★★</option>
            </select>
            <button disabled=${!canSubmit} onclick=${() => addComment()}>${localization.submit}</button>
        </div>
    `
}

render(h`<${App}/>`, document.querySelector(".book-info"));
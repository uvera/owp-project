import {h as html, htm, render, useEffect, useState} from './preact.bundle.js';

const h = htm.bind(html);
const csrf = document.querySelector('meta[id=_csrf]').content;

const qsLang = (elem) => document.querySelector(`#lang-th-${elem}`).textContent

const localization = {
    author: qsLang('author'),
    rating: qsLang('rating'),
    date: qsLang('date'),
    bookName: qsLang('bookname'),
    text: qsLang('text'),
    label: qsLang('label'),
    none: qsLang('none'),
}

const POLL_API_INTERVAL_MS = 5000

document.querySelector('#lang-container').remove()

function App() {
    const [comments, setComments] = useState([])
    const [refresh, doRefresh] = useState(true)
    useEffect(() => {
        fetchFromApi()
    }, [refresh])

    useEffect(() => {
        const timer = setInterval(fetchFromApi, POLL_API_INTERVAL_MS)
        return () => clearInterval(timer)
    }, [])

    function fetchFromApi() {
        fetch('/admin-panel/book-comments/api')
            .then(r => r.json())
            .then(r => setComments(r))
            .catch(e => console.log(e))
    }

    if (comments.length === 0)
        return h`
        <h1 style="
            margin: 0 auto 1rem auto;
            text-align: center;
        ">${localization.none}</h1>
        `
    else
        return h`
            <h1 style="
            margin: 0 auto 1rem auto;
            text-align: center;
            ">${localization.label}</h1>
            <table>
                <tbody>
                    <${Rows} comments=${comments} fresh=${[refresh, doRefresh]}/>
                </tbody>
            </table>
            `
}

function Rows({comments = [], fresh}) {
    return h`
    ${comments.map(e =>
        h`
        <tr class="tr-flex">
            <td>${localization.author} : ${e.author}</td>
            <td>${localization.rating} : ${e.rating}</td>
            <td>${localization.date} : ${e.dateOfPosting}</td>
            <td>${localization.bookName} : ${e.bookName}</td>
            <td>${localization.text} : <p>${e.text}</p></td>
            <td><${DecisionButtons} keys=${[e.author, e.bookIsbn]} fresh=${fresh}/></td>
        </tr>
        `
    )}
    `
}

function DecisionButtons({keys, fresh}) {
    const [author, bookIsbn] = keys
    const [refresh, doRefresh] = fresh

    const performRefresh = () => {
        doRefresh(!refresh)
    }

    function sendDecision(accept = true) {
        fetch('/admin-panel/book-comments/api', {
            method: 'PUT',
            headers: {
                'X-CSRF-TOKEN': csrf,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({author, bookIsbn, accept})
        })
            .then(r => {
                if (r.status === 200)
                    performRefresh()
            })
            .catch(e => console.log(e))
    }

    return h`
    <div>
    <button class="tr-button green" onclick=${() => sendDecision()}>✓</button>
    <button class="tr-button red" onclick=${() => sendDecision(false)}>🗙</button>
</div>
    `
}

render(h`<${App}/>`, document.querySelector("main"));
const items = document.querySelectorAll(".btn-remove-wish")
items.forEach(e => e.addEventListener('click', dropFromWishList))
const csrfToken = document.querySelector("meta[id=_csrf]").content

async function dropFromWishList(event) {
    const isbn = event.target.dataset.isbn
    await fetch(`/self/wishlist/${isbn}`,
        {
            method: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': csrfToken,
                'Content-Type': 'application/json',
            }
        })
    const parent2nd = event.target.parentNode.parentNode;
    const tbody = parent2nd.parentNode;
    if (tbody.childElementCount > 1) {
        parent2nd.remove();
    } else {
        tbody.parentNode.parentNode.remove();
    }

}
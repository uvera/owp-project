const qs = (param) => document.querySelector(param)
const orderButton = qs('#order-button')
const orderModal = qs('#order-modal')
const orderSubmit = qs('#order-submit')
const closeButton = qs('#close')
const amountField = qs('input[id*=amount]')
const isbn = orderSubmit.dataset.isbn
const csrf = qs('#order-form input[name=_csrf]').value

orderButton.addEventListener('click', _ => orderModal.style.display = 'block')
closeButton.addEventListener('click', _ => orderModal.style.display = 'none')

window.addEventListener('click', (event) => {
    if (event.target === orderModal)
        orderModal.style.display = 'none'
})

const submitOrder = async () => {
    const value = amountField.value
    const response = await fetch(`/book/edit/order/${isbn}`, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': csrf,
            'Content-Type': 'application/json',
        },
        body:
            JSON.stringify({value})

    })

    const popAlert = (spanQuery, className, val = "") => {
        qs('#alertH1')?.remove()
        const h1 = document.createElement('h1')
        h1.id = 'alertH1'
        h1.style.cursor = 'pointer'
        h1.classList.add(className)
        h1.addEventListener('click', e => e.target.remove())
        const spanText = qs(spanQuery).cloneNode(true)
        spanText.style.display = 'inline'
        if (val != null && val.length !== 0)
            spanText.innerHTML += ` ${val}`
        h1.append(spanText)
        qs('label[for=amount]').before(h1)
    }

    if (response.status === 202) {
        popAlert('#order-success-span', 'update-success-fast', value)
    } else if (response.status === 400 || response.status === 404) {
        popAlert('#order-failure-span', 'update-failure-fast')
    } else {
        console.log('undefined response status')
    }

}

orderSubmit.addEventListener('click', submitOrder)


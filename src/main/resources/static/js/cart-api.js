const map = new Map()
const token = document.querySelector('meta[name=_csrf]').content

const changeEvent = async (e) => {
    e.preventDefault();
    const target = e.target;
    let value = target.value;
    const isbn = target.dataset.isbn;
    const prevVal = map.get(isbn)
    if (!value.match(/[0-9]/g) || value < 1) {
        if (prevVal != null)
            value = map.get(isbn)
        else
            value = 1
        e.target.value = value;
        return
    } else {
        map.set(isbn, value)
    }
    if (prevVal != null && prevVal === value)
        return
    await fetch(`/api/cart/${isbn}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': token
        },
        body: JSON.stringify({
            amount: value
        })
    })
    const price = target.parentNode.parentNode.querySelector('td[id=price]').innerHTML.split('.')[0].match(/\d/g).join('')
    let totalPrice = price * value
    totalPrice = Math.round(totalPrice)
    const totalPriceNode = target.parentNode.parentNode.querySelector('td[id=totalPrice]')
    const textPart = totalPriceNode.innerHTML.split(':')[0]
    totalPriceNode.innerHTML = `${textPart}: ${totalPrice}.0`
    totalPriceNode.style.color = `${randomColor()}`
}

function randomColor() {
    const colors = ['red', 'blue', 'green', 'magenta', 'cyan']
    return colors[Math.floor(Math.random() * colors.length)]
}

const checkCart = () => {
    const tbody = document.querySelector('tbody')
    if (tbody.childElementCount === 0)
        window.location.reload()
}

const removeCartItem = async (e) => {
    const button = e.target;
    const isbn = button.dataset.isbn;
    map.delete(isbn)
    await fetch(`/api/cart/${isbn}`, {
        method: 'DELETE',
        headers: {
            'X-CSRF-TOKEN': token
        }
    })
    document.querySelectorAll('tbody tr').forEach(e => {
        const btn = e.querySelector('button')
        if (btn.dataset.isbn === isbn)
            e.remove()
    })

    checkCart()
}
const executePurchase = async (e) => {

    const pickLocalise = (name) => document.querySelector(`#mths #mth-${name}`).innerText;

    const inputPoints = document.querySelector('#input-points')?.value ?? 0;
    const maxPoints = document.querySelector('#input-points').getAttribute("max")
    const useAmountOfPoints = inputPoints > maxPoints ? maxPoints : inputPoints
    document.querySelector("#input-points").value = useAmountOfPoints
    const res = await fetch(`/api/shop`, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({useAmountOfPoints})
    })
    let swalRes;
    switch (res.status) {
        case 406:
            swalRes = swal(pickLocalise('book-not-available'))
            break;
        case 404:
            swalRes = swal(pickLocalise('cart-empty'))
            break;
        case 200:
            swalRes = swal(pickLocalise('success'))
            break;
        case 400:
            swalRes = swal(pickLocalise('bad-req'))
            break;
        default:
            break;
    }
    swalRes.then(() =>
        window.location.reload()
    )
}

const removeFromCart = document.querySelectorAll(".cart-removal-js");
removeFromCart.forEach(e => e.addEventListener('click', removeCartItem))

const amountInputs = document.querySelectorAll("input[name=amount]")
amountInputs.forEach(e => e.addEventListener('change', changeEvent))

const executeButton = document.querySelector('#execute-button')
executeButton?.addEventListener('click', executePurchase)


let errorModal = document.querySelector('#errorModal')
let close = document.querySelector('#close')

close.addEventListener
('click', () => {
    errorModal.style.display = 'none'
    console.log('clicked')
})

window.addEventListener('click', (event) => {
    if (event.target === errorModal)
        errorModal.style.display = 'none'
})

const addToCart = async (e) => {
    e.preventDefault();
    const isbn = e.target.dataset.isbn;
    const response = await fetch(`/api/cart/${isbn}`, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': document.querySelector("meta[name=_csrf]").content
        }
    })
    const st = response.status
    switch (st) {
        case 200:
            window.location.href = `/cart`
            break
        case 400:
            e.target.backgroundColor = `#BB0505`
            break
        default:
            console.log('API: cart: undefined response status')
            break
    }

}

document.querySelector('#btn-cart')
    ?.addEventListener('click', addToCart);

const addToWishList = async (e) => {
    e.preventDefault();
    const isbn = e.target.dataset.isbn;
    const response = await fetch(`/self/wishlist/${isbn}`, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': document.querySelector("meta[name=_csrf]").content
        }
    })
    const st = response.status
    switch (st) {
        case 200:
            window.location.href = `/profile`
            break
        case 400:
            e.target.backgroundColor = `#BB0505`
            break
        default:
            console.log('API: cart: undefined response status')
            break
    }

}

document.querySelector('#btn-wishlist')
    ?.addEventListener('click', addToWishList);

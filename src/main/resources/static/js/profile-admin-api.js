const csrfToken = document.querySelector("meta[id=_csrf]").content
const roleSelect = document.querySelector("#roleselect")
const activeSelect = document.querySelector("#activeselect")

async function roleChange(event) {
    const value = event.target.value
    const username = event.target.dataset.username
    const response = await fetch(`/profile/${username}/role`, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': csrfToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({value})
    })
    if (response.status === 200) {
        if (value === 'true')
            activeSelect.setAttribute('disabled', 'true')
        else
            activeSelect.removeAttribute('disabled')
    }
}

if (roleSelect)
    roleSelect.addEventListener('change', roleChange)

async function statusChange(event) {
    const value = event.target.value
    const username = event.target.dataset.username
    const response = await fetch(`/profile/${username}/active`, {
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': csrfToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({value})
    })
    console.log(response.status)
}

if (activeSelect)
    activeSelect.addEventListener('change', statusChange);

(function () {
    if (roleSelect?.value === 'true') {
        activeSelect.setAttribute('disabled', 'true')
    }
})()
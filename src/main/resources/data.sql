INSERT INTO Book
VALUES ('JavaScript funkcionalno programiranje',
        '9788673105505',
        'Kompjuter biblioteka',
        '2020',
        'Funkcionalno programiranje je paradigma za razvoj softvera sa boljim performansama. Ono pomaže da napišete
sažet kod i kod koji se može testirati. Da biste podigli vaše programske veštine na viši nivo, ova sveobuhvatna knjiga će vam pomoći da iskoristite mogućnosti funkcionalnog programiranja u JavaScriptu i da napišete visokoodržive i testirane aplikacije za Veb i server pomoću funkcionalnog JavaScripta.',
        2310.0,
        470,
        'soft',
        'latin',
        'Srpski',
        65) ^;

INSERT INTO Book
VALUES ('Naučite Bootstrap 4',
        '9788673105260',
        'Kompjuter biblioteka',
        '2018',
        'Ova knjiga će vam pomoći da upotrebite i prilagodite Bootstrap za kreiranje privlačnih veb sajtova koji
odgovaraju vašim potrebama. Kreiraćete prilagođeni Bootstrap veb sajt korišćenjem različitih pristupa za prilagođavanje radnog okvira. Koristićete ključne funkcije Bootstrapa i brzo ćete otkriti različite načine na koje Bootstrap može da vam pomogne da kreirate veb interfejse.',
        1760.0,
        354,
        'hard',
        'latin',
        'Srpski',
        123) ^;

INSERT INTO Author
VALUES ('9788673105505', 'Federico Kereki'),
       ('9788673105505', 'Another Author'),
       ('9788673105260', 'Benjamin Jakobus') ^;

INSERT INTO Genre (name, description)
VALUES ('Science', 'Genre is about all types of science.'),
       ('Computers', 'Genre about computer stuff. :D.'),
       ('Programming', 'Genre about computer beep boop.'),
       ('Drama', 'Genre is about drama, lol.') ^;

INSERT INTO BooksGenres
VALUES ('9788673105505', (SELECT GetGenreIdByName('Science'))),
       ('9788673105505', (SELECT GetGenreIdByName('Computers'))),
       ('9788673105505', (SELECT GetGenreIdByName('Programming'))),
       ('9788673105260', (SELECT GetGenreIdByName('Computers'))),
       ('9788673105260', (SELECT GetGenreIdByName('Science'))),
       ('9788673105260', (SELECT GetGenreIdByName('Programming')))
    ^;

INSERT INTO User
(username, password, email, firstName, lastName, dateOfBirth, address, phoneNumber, role)
VALUES ('dusan',
        '{noop}dusan123',
        'dusan.uveric9@gmail.com',
        'Dusan',
        'Uveric',
        '2000-09-17',
        'Adresa 1',
        '06000000',
        'admin'),
       ('petar',
        '{noop}petar123',
        'petar@email.com',
        'Petar',
        'Petric',
        '1979-02-25',
        'Karadjordjeva 60',
        '0654625530',
        'user'),
       ('ivan',
        '{noop}ivan123',
        'ivan@email.com',
        'Ivan',
        'Ivanovic',
        '1989-06-15',
        'Karadjordjeva 59',
        '0644121620',
        'user'),
       ('aleksandar',
        '{noop}aleksandar123',
        'aleks@email.com',
        'Aleksandar',
        'Marjanovic',
        '2000-05-03',
        'Bulevar evrope 50',
        '0663218821',
        'user') ^;

INSERT INTO LoyaltyCardRequest (username, status)
VALUES ('petar', 'accepted') ^;

INSERT INTO LoyaltyCard
VALUES ('petar',
        4) ^;

INSERT INTO Purchase (username)
VALUES ('petar') ^;

INSERT INTO BoughtBook (bookIsbn, amountOfBooks, price, purchaseId)
VALUES ('9788673105505',
        3,
        4000.0,
        1),
       ('9788673105260',
        4,
        3000.0,
        1) ^;

INSERT INTO LoyaltyCardRequest(username)
VALUES ('ivan')
    ^;

UPDATE LoyaltyCardRequest
SET status = 'rejected'
WHERE username = 'ivan'^;

INSERT INTO Purchase (username)
VALUES ('ivan') ^;

INSERT INTO BoughtBook (bookIsbn, amountOfBooks, price, purchaseId)
VALUES ('9788673105505',
        3,
        4000.0,
        2),
       ('9788673105260',
        4,
        3000.0,
        2) ^;

INSERT INTO Comment (text, rating, author, bookIsbn, status, dateOfPosting)
VALUES ('Okej je knjiga!',
        3,
        'ivan',
        '9788673105505',
        'accepted',
        '2020-12-14'),
       ('Ne svidja mi se knjiga ali je okej!',
        2,
        'ivan',
        '9788673105260',
        'pending',
        '2020-12-14') ^;

/***/
INSERT INTO Purchase (username, dateOfPayment)
VALUES ('aleksandar', '2020-12-20') ^;

INSERT INTO BoughtBook (bookIsbn, amountOfBooks, price, purchaseId)
VALUES ('9788673105260',
        7,
        3000.0,
        3) ^;

INSERT INTO Comment (text, rating, author, bookIsbn, status, dateOfPosting)
VALUES ('Odlicna knjiga!',
        5,
        'aleksandar',
        '9788673105505',
        'pending',
        '2019-12-24'),
       ('Okej knjiga!',
        3,
        'aleksandar',
        '9788673105260',
        'accepted',
        '2019-12-25')
    ^;
/***/

INSERT INTO BookSale (percentDown, bookIsbn, saleDate)
VALUES (40, '9788673105505', '2021-01-05'),
       (30, '9788673105505', '2021-02-07')
    ^;